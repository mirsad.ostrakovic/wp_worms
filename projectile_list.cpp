#include "projectile_list.hpp"

std::list<ProjectileState> ProjectileList::checkTargetHit()
 {
    std::list<ProjectileState> hitTargetProjectileList;

    for(auto it = std::begin(projectileList); it != std::end(projectileList); ++it)
        if(it->isHit(targetBITMAP))
        {
            hitTargetProjectileList.push_back(std::move(*it));
            it = projectileList.erase(it);
        }

    return hitTargetProjectileList;
 }

void ProjectileList::animate()
{
    for(auto& elem : projectileList)
        elem.update();
}

void ProjectileList::draw(HDC hdc)
{
    for(auto& elem : projectileList)
        elem.render(hdc);
}
