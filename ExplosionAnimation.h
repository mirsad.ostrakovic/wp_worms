#ifndef EXPLOSIONANIMATION_H_INCLUDED
#define EXPLOSIONANIMATION_H_INCLUDED

#include <windows.h>

class ExplosionAnimation;

class ExplosionAnimationListener{
    public:

        ExplosionAnimationListener() = default;
        virtual ~ExplosionAnimationListener() = default;
        virtual void registerExplosionAnimation(ExplosionAnimation *explosionAnimation) = 0;
        virtual void unregisterExplosionAnimation(ExplosionAnimation *explosionAnimation) = 0;
};


class ExplosionAnimation{
    public:
        ExplosionAnimation(ExplosionAnimationListener *listener) : explosionAnimationListener{listener} { explosionAnimationListener->registerExplosionAnimation(this); }
        virtual ~ExplosionAnimation() = default;
        virtual void animate(HDC) = 0;

    protected:
        void unregisterAnimation() { explosionAnimationListener->unregisterExplosionAnimation(this); }
        ExplosionAnimationListener *explosionAnimationListener;

};





#endif // EXPLOSIONANIMATION_H_INCLUDED
