#include "health_bar.h"

void HealthBar::draw(HDC bufferHDC, int xPos, int yPos, int width, int height, COLORREF color, float percent, const std::string& userNickname)
{
  if(percent < 0.0)
    percent = 0.0;

  if(percent > 100.0)
    percent = 100.0;

  HBRUSH oldBRUSH = (HBRUSH)SelectObject(bufferHDC, GetStockObject(BLACK_BRUSH));
  Rectangle(bufferHDC, xPos, yPos, xPos + width, yPos + height);

  SelectObject(bufferHDC, GetStockObject(WHITE_BRUSH));
  Rectangle(bufferHDC, xPos + width*0.03, yPos + height*0.1, xPos + width*0.97, yPos + height*0.9);

  HBRUSH redBRUSH = CreateSolidBrush(color);
  SelectObject(bufferHDC, redBRUSH);
  if(percent > 0)
    Rectangle(bufferHDC, xPos + width*0.02, yPos + height*0.1, xPos + width*0.98*percent/100.0 , yPos + height*0.9);

  SetBkMode(bufferHDC, TRANSPARENT);

  //display user nickname
  SIZE dim;
  GetTextExtentPoint32(bufferHDC, userNickname.c_str(), userNickname.size(), &dim);

  TextOut(bufferHDC,
          xPos + width/2  - dim.cx/2,
          yPos + height/2 - dim.cy/2,
          userNickname.c_str(),
          userNickname.size());



  SelectObject(bufferHDC, oldBRUSH);
  DeleteObject(redBRUSH);
}
