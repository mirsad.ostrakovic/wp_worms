#include "worms.hpp"



WormsState::WormsState(int x, int y, int w, int h, RECT moveRect, bool dir,  const std::string& nickname):
      xPos{ x },
      yPos{ y },
      width{ w },
      height{ h },
      moveRectangle{ moveRect },
      direction{ dir },
      userNickname{ nickname }
{
  if( xPos - width/2 < moveRectangle.left || xPos + width/2 > moveRectangle.right )
  {
    std::cout << "xPos: " << xPos << std::endl;
    std::cout << "width/2: " << width/2 << std::endl;
    std::cout << "left: " << moveRect.left << std::endl;
    std::cout << "right: " << moveRect.right << std::endl;
    throw std::invalid_argument("WormsState(): invalid worms x position: " + std::to_string(x));
  }



  if( yPos - height/2 < moveRectangle.top || yPos + height/2 > moveRectangle.bottom )
    throw std::invalid_argument("WormsState(): invalid worms y position: " + std::to_string(y));
}


void WormsState::moveUP(std::size_t n){
  yPos -= n;
  if(yPos - height/2 < moveRectangle.top)
    yPos = moveRectangle.top + height/2;
}



void WormsState::moveDOWN(std::size_t n){
  yPos += n;
  if(yPos + height/2 > moveRectangle.bottom)
    yPos = moveRectangle.bottom - height/2;
}



void WormsState::moveLEFT(std::size_t n){
  xPos -= n;
  if(xPos - width/2 < moveRectangle.left)
    xPos = moveRectangle.left + width/2;
  direction = false;
  dynamicAnimationCounter++;
}



void WormsState::moveRIGHT(std::size_t n){
  xPos += n;
 if(xPos + width/2 > moveRectangle.right)
    xPos = moveRectangle.right - width/2;
  direction = true;
  dynamicAnimationCounter++;
}





void WormsSprite::draw(HDC bufferHDC, int xPos, int yPos, std::size_t spriteCount, bool inverted)
{
  spriteCount = spriteCount % (columns * rows);

  int posX = (spriteCount) % columns * getWidth();
  int posY = (spriteCount) / columns * getHeight();

  HDC tmpHDC = CreateCompatibleDC(bufferHDC);
  HBITMAP oldBITMAP = (HBITMAP)SelectObject(tmpHDC, imgBitmask);

  if(inverted)
    StretchBlt(bufferHDC, xPos, yPos, getWidth(), getHeight(), tmpHDC, posX + getWidth() - 1, posY, -getWidth(), getHeight(), SRCAND);
  else
    BitBlt(bufferHDC, xPos, yPos, getWidth(), getHeight(), tmpHDC, posX, posY, SRCAND);

  SelectObject(tmpHDC, img);

  if(inverted)
    StretchBlt(bufferHDC, xPos, yPos, getWidth(), getHeight(), tmpHDC, posX + getWidth() - 1, posY, -getWidth(), getHeight(), SRCPAINT);
  else
    BitBlt(bufferHDC, xPos, yPos, getWidth(), getHeight(), tmpHDC, posX, posY, SRCPAINT);


  SelectObject(tmpHDC, oldBITMAP);
  DeleteDC(tmpHDC);
}



WormsSprite::WormsSprite(const std::string& filenameImg,
                         const std::string& filenameBitmaskImg,
                         int width,
                         int height,
                         int c,
                         int r):
      img{ (HBITMAP)LoadImage(NULL, filenameImg.c_str(), IMAGE_BITMAP, width, height, LR_LOADFROMFILE) },
      imgBitmask{ (HBITMAP)LoadImage(NULL, filenameBitmaskImg.c_str(), IMAGE_BITMAP, width, height, LR_LOADFROMFILE) },
      columns{ c },
      rows{ r }
{
  if(img == NULL)
    throw std::invalid_argument("WormsSprite(): image " + filenameImg + " not found");

  if(imgBitmask == NULL)
    throw std::invalid_argument("WormsSprite(): image " + filenameBitmaskImg + " not found");

  BITMAP imgBitmaskInfo;
  GetObject(imgBitmask, sizeof(imgBitmaskInfo), &imgBitmaskInfo);
  GetObject(img, sizeof(imgInfo), &imgInfo);

  if(imgBitmaskInfo.bmWidth != imgInfo.bmWidth || imgBitmaskInfo.bmHeight != imgInfo.bmHeight)
    throw std::invalid_argument("WormsSprite(): incompatibile dimensions of img and bitmaskImg");
}

 WormsSprite::WormsSprite(const WormsSprite& ws):
    img{ ws.img },
    imgBitmask{ ws.imgBitmask },
    columns{ ws.columns },
    rows{ ws.rows },
    imgInfo{ ws.imgInfo }
 {

 }



void WormsType::draw(HDC bufferHDC,
                 int xPos,
                 int yPos,
                 std::size_t staticAnimationCount,
                 bool inverted,
                 std::size_t dynamicAnimationCount)
{
    wormsSprites.at(dynamicAnimationCount%wormsSprites.size()).draw(bufferHDC, xPos, yPos, staticAnimationCount, inverted);
}


void WormsType::addSprite(WormsSprite&& sprite)
{
    if(wormsSprites.size() != 0u)
    {
        if(wormsSprites.at(0).getHeight() != sprite.getHeight() ||
           wormsSprites.at(0).getWidth() != sprite.getWidth() ||
           wormsSprites.at(0).getColumnCount() != sprite.getColumnCount() ||
           wormsSprites.at(0).getRowsCount() != sprite.getRowsCount())
        {
            throw std::invalid_argument("WormsType::addSprite(): incompatible sprites format");
        }

        wormsSprites.push_back(sprite);
    }

}


int WormsType::getWidth() const
{
    if(wormsSprites.size() == 0u) return -1;
    return wormsSprites.at(0).getWidth();
}


int WormsType::getHeight() const
{
  if(wormsSprites.size() == 0u) return -1;
    return wormsSprites.at(0).getHeight();
}

void WormsState::moveToPoint(POINT pt)
{
    if(pt.x > xPos)
        direction = true;
    else if(pt.x < xPos)
        direction = false;

    xPos = pt.x;
    yPos = pt.y;
    dynamicAnimationCounter++;
}

POINT WormsState::getCurrentPosition() const
{
    POINT pt;
    pt.x = xPos;
    pt.y = yPos;

    return pt;
}
