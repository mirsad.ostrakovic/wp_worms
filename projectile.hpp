#ifndef PROJECTILE_HPP_INCLUDED
#define PROJECTILE_HPP_INCLUDED

#include <windows.h>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cmath>
#include "vector2D.hpp"
#include "game_object.hpp"
#include "background.hpp"
#include <wingdi.h>

using std::min;
using std::max;

class ProjectileType{
  public:
    ProjectileType(const std::string& imgFilename,
                   const std::string& imgBitmaskFilename,
                   int width, int height);

    void draw(HDC bufferHDC, int xPos, int yPos, float radians) const;

    int getWidth() const { return imgInfo.bmWidth; }
    int getHeight() const { return imgInfo.bmHeight; }

    bool isOverlapping(HBITMAP bitmask, int xPos, int yPos, float radians) const;

    friend class ProjectileState;

    const static double airResistance;
    const static Vector2D gravitationalAcceleration;
    const static double explosionRadius;

    static double getMinimumRange() { return minimumRange; }
    static double getMaximumRange() { return maximumRange; }


  private:
    HBITMAP img;
    HBITMAP imgBitmask;
    BITMAP imgInfo;
    const static double minimumRange;
    const static double maximumRange;

};

class ProjectileState : public GameObject{
    public:

        ProjectileState(const ProjectileType& proj,
                        const Background& bg,
                        double xPos, double yPos,
                        double xVelocity = 0.0,
                        double yVelocity = 0.0):
            projectile{proj},
            background{bg}
        {
            _position = Vector2D(xPos, yPos);
            _velocity = Vector2D(xVelocity, yVelocity);
            _acceleration = Vector2D(0, 0);
        }

        ProjectileState(const ProjectileState& ps):
            projectile{ ps.projectile },
            background{ ps.background }
        {
            _position = ps._position;
            _velocity = ps._velocity;
            _acceleration = ps._acceleration;
        }

        void update();
        void render(HDC hdc);
        void checkInput();

        bool isHit(HBITMAP bitmask) const;
        POINT getTopOfRocketLocation() const;

        void increaseAngle() { _velocity.SetAngle(_velocity.GetAngle() - 0.02); }
        void decreaseAngle() { _velocity.SetAngle(_velocity.GetAngle() + 0.02); }


        double getMinimumRange() const { return projectile.getMinimumRange(); }
        double getMaximumRange() const { return projectile.getMaximumRange(); }
        double getRange() const { return _velocity.GetLength(); }
        void increaseRange();  // { _velocity.SetLength(_velocity.GetLength() + 0.2); }
        void decreaseRange(); //  { _velocity.SetLength(_velocity.GetLength() - 0.2); }

        double getExplosionRadius() const { return projectile.explosionRadius; }

    private:
        const ProjectileType& projectile;
        const Background& background;
};






#endif // PROJECTILE_HPP_INCLUDED
