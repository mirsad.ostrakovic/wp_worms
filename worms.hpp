#ifndef WORMS_HPP_INCLUDED
#define WORMS_HPP_INCLUDED

#include <string>
#include <stdexcept>
#include <vector>
#include <iostream>
#include <windows.h>




class WormsSprite{
  public:
    WormsSprite(const std::string& filenameImg,
                const std::string& filenameBitmaskImg,
                int width,
                int height,
                int c,
                int r);

    WormsSprite(const WormsSprite&);

    int getWidth() const { return imgInfo.bmWidth / columns; }
    int getHeight() const { return imgInfo.bmHeight / rows; }

    int getColumnCount() const { return columns; }
    int getRowsCount() const { return rows; }

    void draw(HDC bufferHDC, int xPos, int yPos, std::size_t spriteCount,  bool inverted);


  private:
    HBITMAP img;
    HBITMAP imgBitmask;
    BITMAP imgInfo;
    int columns;
    int rows;
};



class WormsType{
    public:
        WormsType() = default;

        WormsType(std::initializer_list<WormsSprite> l) : wormsSprites{l} {}

         void draw(HDC bufferHDC,
                   int xPos,
                   int yPos,
                   std::size_t staticAnimationCount,
                   bool inverted,
                   std::size_t dynamicAnimationCount);

        void addSprite(WormsSprite&& sprite);

        int getWidth() const;
        int getHeight() const;


    private:
        std::vector<WormsSprite> wormsSprites;
};



class WormsState{
  public:
    WormsState() = default;
    WormsState(int x, int y, int w, int h, RECT moveRect, bool direction, const std::string& nickname);

    void moveUP(std::size_t n);
    void moveDOWN(std::size_t n);
    void moveLEFT(std::size_t n);
    void moveRIGHT(std::size_t n);
    void moveToPoint(POINT pt);

    int getX() const { return xPos; }
    int getY() const { return yPos; }
    POINT getCurrentPosition() const;

    bool getDirection() const { return direction; }

    std::size_t getDynamicAnimationCounter() const { return dynamicAnimationCounter; }

    void increaseHealth(float h) { health += h; if(health > 100.0) health = 100.0; }
    void decreaseHealth(float h) { health -= h; if(health < 0.0) health = 0.0; }
    float getHealth() const { return health; }

    const std::string& getUserNickname() const { return userNickname; }

    void setFreeFalling() { isFreeFalling = true;}
    void resetFreeFalling() { isFreeFalling = false; }
    bool getFreeFallingState() const { return isFreeFalling; }

  private:
    int xPos;
    int yPos;
    int width;
    int height;
    RECT moveRectangle;
    bool direction;
    std::size_t dynamicAnimationCounter = 0u;
    float health = 100.0;
    const std::string userNickname;
    bool isFreeFalling = false;
};





#endif // WORMS_HPP_INCLUDED
