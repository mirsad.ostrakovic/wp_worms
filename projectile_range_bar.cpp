#include "projectile_range_bar.hpp"

void ProjectileRangeBar::draw(HDC bufferHDC, int xPos, int yPos, int width, int height, COLORREF color, double value, const double minimumValue, const double maximumValue)
{

  if(value < minimumValue)
     value = minimumValue;

  if(value > maximumValue)
    value = maximumValue;

  double percent = (value - minimumValue)/(maximumValue-minimumValue);

  HBRUSH oldBRUSH = (HBRUSH)SelectObject(bufferHDC, GetStockObject(BLACK_BRUSH));
  Rectangle(bufferHDC, xPos, yPos, xPos + width, yPos + height);

  SelectObject(bufferHDC, GetStockObject(WHITE_BRUSH));
  Rectangle(bufferHDC, xPos + width*0.03, yPos + height*0.1, xPos + width*0.97, yPos + height*0.9);

  HBRUSH colorBRUSH = CreateSolidBrush(color);
  SelectObject(bufferHDC, colorBRUSH);

  if(value > minimumValue)
    Rectangle(bufferHDC, xPos + width*0.02, yPos + height*0.1, xPos + width*0.98*percent , yPos + height*0.9);


  SelectObject(bufferHDC, oldBRUSH);
  DeleteObject(colorBRUSH);
}

