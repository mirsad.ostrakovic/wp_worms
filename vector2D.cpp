#include "vector2D.hpp"

Vector2D::Vector2D():
  _x{0},
  _y{0}
{}

Vector2D::Vector2D(double x, double y):
  _x{x},
  _y{y}
{}

void Vector2D::SetAngle(double angle)
{
  double r = sqrt(_x*_x + _y*_y);
  _x = r * cos(angle);
  _y = r * sin(angle);
}

double Vector2D::GetAngle() const
{
  return atan2(_y, _x);
}

void Vector2D::SetLength(double length)
{
  double angle = atan2(_y, _x);
  _x = length * cos(angle);
  _y = length * sin(angle);
}

double Vector2D::GetLength() const
{
  return sqrt(_x*_x + _y*_y);
}


Vector2D Vector2D::Add(const Vector2D& other)
{
  return Vector2D(_x + other._x, _y + other._y);
}

Vector2D Vector2D::Substract(const Vector2D& other)
{
  return Vector2D(_x - other._x, _y - other._y);
}

Vector2D Vector2D::Multiply(int value)
{
  return Vector2D(_x * value, _y * value);
}

Vector2D Vector2D::Divide(int value)
{
    return Vector2D(_x / value, _y / value);
}

void Vector2D::AddTo(const Vector2D& other)
{
  _x += other._x;
  _y += other._y;
}

void Vector2D::SubstractFrom(const Vector2D& other)
{
  _x -= other._x;
  _y -= other._y;
}

void Vector2D::MultiplyBy(int value)
{
  _x *= value;
  _y *= value;
}

void Vector2D::DivideBy(int value)
{
  _x /= value;
  _y /= value;
}

POINT Vector2D::getPOINT() const
{
  POINT pt;
  pt.x = _x;
  pt.y = _y;

  return pt;
}

Vector2D::Vector2D(POINT pt):
  _x{(double)pt.x},
  _y{(double)pt.y}
{}
