#ifndef GAME_INSTANCE_H_INCLUDED
#define GAME_INSTANCE_H_INCLUDED

#include <vector>
#include <memory>
#include <iostream>
#include <tuple>

#include "background.hpp"
#include "landspace.hpp"
#include "projectile.hpp"
#include "worms.hpp"
#include "projectile_list.hpp"
#include "landscape_line.hpp"
#include "free_fall.hpp"
#include "explosion_animation_v1.hpp"

class GameInstance{
    public:
        GameInstance(Background& bg, Landscape& ls):
            background{ bg },
            landscape{ ls }
        {}

        Landscape& getLandscape() { return landscape; }
        Background& getBackground() { return background; }

        void addProjectileType(ProjectileType& pt) { projectileTypeList.push_back(pt); }
        ProjectileType& getProjectileType(std::size_t idx) { return projectileTypeList.at(idx); }

        void addWormsType(WormsType& wt) { wormsTypeList.push_back(wt); }
        WormsType& getWormsType(std::size_t idx) { return wormsTypeList.at(idx); }

        ProjectileList& getProjectileList() { return projectileList; }
        ExplosionAnimationList& getExplosionList() { return explosionList; }

        void addWorms(const WormsState& ws) { wormsList.push_back(ws); }
        void addWorms(WormsState& ws) { wormsList.push_back(ws); }
        WormsState& getWorms(std::size_t idx) { return wormsList.at(idx); }
        std::vector<WormsState>& getWormsList() { return wormsList; }

        WormsState& getWormsOnTurn() { return wormsList.at(currentWormsTurn); }
        void setNextTurnDelayed() { isNextTurnWithDelay = true; }
        void resetNextTurnDelayed() { isNextTurnWithDelay = false; }
        bool isNextTurnWithDelaySet() const { return isNextTurnWithDelay; }
        void nextTurn() { currentWormsTurn = (currentWormsTurn + 1) % wormsList.size(); }
        std::size_t getWormsTurn() const { return currentWormsTurn; }


        void setNewProjectileToReadyState(ProjectileState& ps) { projectileReadyToLaunch = std::make_unique<ProjectileState>(ps); }
        std::unique_ptr<ProjectileState>& getProjectileReadyToLaunch() { return projectileReadyToLaunch; }
        void launchReadyProjectile() { projectileList.addNew(*projectileReadyToLaunch); projectileReadyToLaunch.reset(); }
        bool isProjectileReadyToLunch() const { return projectileReadyToLaunch != nullptr; }

        bool isEndedAnimationScene() const { return projectileList.getProjectileCount() == 0u && explosionList.getExplosionCount() == 0u && freeFallList.empty();}

        /*
        void calculateLandscapeLineList(HBITMAP landscapeBITMAP)
        {
            std::ofstream myfile;
            myfile.open ("example2.txt");

            landscapeLineList = std::move(getLandspaceMovingLines(landscapeBITMAP));


            for(auto& elem : landscapeLineList)
                myfile << elem << std::endl;

            myfile.close();
        }
        */

        void calculateLandscapeLineList(HBITMAP landscapeBITMAP) { landscapeLineList = std::move(getLandspaceMovingLines(landscapeBITMAP)); }


        std::vector<LandscapeLine>& getLandscapeLineList() { return landscapeLineList; }
        std::list<FreeFallState>& getFreeFallList() { return freeFallList; }

        std::tuple<bool,WormsState&> isGameFinished();




    private:
        Background background;
        Landscape landscape;
        std::vector<ProjectileType> projectileTypeList;
        std::vector<WormsType> wormsTypeList;
        ProjectileList projectileList;
        ExplosionAnimationList explosionList;

        std::vector<WormsState> wormsList;
        std::size_t currentWormsTurn = 0u;
        bool isNextTurnWithDelay = false;

        std::unique_ptr<ProjectileState> projectileReadyToLaunch;
        std::vector<LandscapeLine> landscapeLineList;
        std::list<FreeFallState> freeFallList;
        //bool isProjectileReadyToLunchState;

};



#endif // GAME_INSTANCE_H_INCLUDED
