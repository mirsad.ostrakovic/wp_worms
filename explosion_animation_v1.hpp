#ifndef EXPLOSION_ANIMATION_V1_HPP_INCLUDED
#define EXPLOSION_ANIMATION_V1_HPP_INCLUDED

#include <list>
#include <vector>
#include "worms.hpp"
#include "ExplosionAnimation.h"
#include "explosion_animation_v1.hpp"

class Explosion{
    public:
        Explosion(int x, int y, float r):
            xPos{x},
            yPos{y},
            radius{r},
            animationRadius{r}
        { PlaySound("explode2.wav", NULL, SND_ASYNC); }

        void animate();
        void impact(std::vector<WormsState>& wormsList);
        void draw(HDC bufferHDC);
        bool isFinished() { return animationRadius <= 0.0; }

    private:
        const int xPos;
        const int yPos;
        const double radius;
        double animationRadius;

};




class ExplosionAnimationList{
    public:
        ExplosionAnimationList() = default;

        std::list<Explosion> clean();
        void animate() { for(auto& elem : explosionList) elem.animate(); }
        void draw(HDC hdc) { for(auto& elem : explosionList) elem.draw(hdc); }
        void impact(std::vector<WormsState>& wormsList) { for(auto& elem : explosionList) elem.impact(wormsList); }
        void addExplosion(const Explosion& explosion){ explosionList.push_back(explosion); }

        std::size_t getExplosionCount() const { return explosionList.size(); }

    private:
        std::list<Explosion> explosionList;
};


#endif // EXPLOSION_ANIMATION_V1_HPP_INCLUDED
