#include "game_loop_functions.hpp"

static int gameInstanceOption()
{
    static int value = (srand(time(NULL)), rand()%2);
    return value;
}


Background background1("background1.bmp", 1600, 800);

Background background2("background2.bmp", 1600, 800);


Landscape landscape1("landspace_bitmask_white2.bmp",
                     "landspace_bitmask_black2.bmp",
                     1600, 800,
                     "texture1.bmp", 400, 300);


Landscape landscape2("landspace_bitmask_white1.bmp",
                     "landspace_bitmask_black1.bmp",
                     1600, 800,
                     "texture2.bmp", 700, 400);

  static int count = 0;


void initGameInstance(const std::vector<std::string>& players)
{
   gameInstance.getProjectileList().registerTargetBitmap(gameInstance.getLandscape().getLandspaceBitmask());

   ProjectileType projectile("rocket_black.bmp", "rocket_white.bmp", 30, 30);

   WormsSprite worms1("worms_sprite1_black.bmp", "worms_sprite1_white.bmp", 60, 720, 1, 12);
   WormsSprite worms2("worms_sprite2_black.bmp", "worms_sprite2_white.bmp", 60, 720, 1, 12);
   WormsSprite worms3("worms_sprite3_black.bmp", "worms_sprite3_white.bmp", 60, 720, 1, 12);
   WormsType worms = {worms1, worms2, worms3};

   gameInstance.addProjectileType(projectile);
   gameInstance.addWormsType(worms);

   int xPos;

   for(auto i = 0u; i < players.size(); ++i)
   {

       do
       {
         xPos = i * gameInstance.getBackground().getWidth()/players.size() + (rand() % (gameInstance.getBackground().getWidth()/players.size()));
         xPos = xPos % gameInstance.getBackground().getWidth();
       }
       while(xPos <= gameInstance.getWormsType(0).getWidth() || xPos >= gameInstance.getBackground().getWidth() - gameInstance.getWormsType(0).getWidth());


        gameInstance.addWorms(WormsState(xPos, findWormsTopLocation(gameInstance.getLandscape().getLandspaceBitmask(), xPos).y,
                                         gameInstance.getWormsType(0).getWidth(), gameInstance.getWormsType(0).getHeight(),
                                         gameInstance.getBackground().getDimensions(),
                                         i < players.size()/2, players.at(i)));
   }

   gameInstance.calculateLandscapeLineList(gameInstance.getLandscape().getLandspaceBitmask());

}


GameInstance gameInstance(gameInstanceOption() ? background1 : background2, gameInstanceOption() ? landscape1 : landscape2);





void updateStates(HWND hwnd)
{

    if(GetAsyncKeyState(VK_ESCAPE) & 0x8000) // for now solution, should add dialoge for close
    {
        PostQuitMessage(0);
        return;
    }

    POINT pt;
    GetCursorPos(&pt);
    ScreenToClient(hwnd, &pt);

    auto gameFinishedResult = gameInstance.isGameFinished();

    if(std::get<0>(gameFinishedResult))
    {
        std::cout << "GAME FINISHED" << std::endl;
        std::cout << "WINNER: " << std::get<1>(gameFinishedResult).getUserNickname() << std::endl;
        return;
    }


    if(gameInstance.isEndedAnimationScene())
    {
        //if((GetAsyncKeyState(VK_LBUTTON) & 0x0001 ? true : false))
        //{
        //    std::cout << "DESTROY LANDSPACE" << std::endl;
        //    gameInstance.getLandscape().destroyLandscape(pt, 150);
        //    gameInstance.calculateLandscapeLineList(gameInstance.getLandscape().getLandspaceBitmask());
        //}

        if((GetAsyncKeyState(VK_LEFT) & 0x8000 ? true : false))
        {
            if(!gameInstance.isProjectileReadyToLunch())
            {
                auto res =  moveLeft(gameInstance.getLandscapeLineList(), gameInstance.getLandscape(), gameInstance.getWormsOnTurn().getCurrentPosition());
                if(std::get<0>(res) == 2)
                {
                    gameInstance.getWormsOnTurn().moveToPoint(std::get<1>(res));
                }
                else if(std::get<0>(res) == 1)
                {
                    std::cout << "add animation" << std::endl;
                    auto nextMovingPOINT = gameInstance.getWormsOnTurn().getCurrentPosition();
                    nextMovingPOINT.x = std::get<1>(res).x;
                    gameInstance.getFreeFallList().push_back(FreeFallState(nextMovingPOINT, std::get<1>(res), gameInstance.getWormsOnTurn()));
                }
                else
                {
                    std::cout << "Error move" << std::endl;
                    exit(-1);
                }
            }
            else
            {
                gameInstance.getProjectileReadyToLaunch()->decreaseRange();
            }
        }

        if((GetAsyncKeyState(VK_RIGHT) & 0x8000 ? true : false))
        {
            if(!gameInstance.isProjectileReadyToLunch())
            {
                auto res =  moveRight(gameInstance.getLandscapeLineList(), gameInstance.getLandscape(), gameInstance.getWormsOnTurn().getCurrentPosition());
                if(std::get<0>(res) == 2)
                {
                    gameInstance.getWormsOnTurn().moveToPoint(std::get<1>(res));
                }
                else if(std::get<0>(res) == 1)
                {
                    std::cout << "add animation" << std::endl;
                    auto nextMovingPOINT = gameInstance.getWormsOnTurn().getCurrentPosition();
                    nextMovingPOINT.x = std::get<1>(res).x;
                    gameInstance.getFreeFallList().push_back(FreeFallState(nextMovingPOINT, std::get<1>(res), gameInstance.getWormsOnTurn()));
                }
                else
                {
                    std::cout << "Error move" << std::endl;
                    exit(-1);
                }
            }
            else
            {
                gameInstance.getProjectileReadyToLaunch()->increaseRange();
            }

        }


        if((GetAsyncKeyState(VK_UP) & 0x8000 ? true : false) && gameInstance.isProjectileReadyToLunch())
        {
            gameInstance.getProjectileReadyToLaunch()->increaseAngle();
        }

        if((GetAsyncKeyState(VK_DOWN) & 0x8000 ? true : false) && gameInstance.isProjectileReadyToLunch())
        {
            gameInstance.getProjectileReadyToLaunch()->decreaseAngle();
        }


        if(GetAsyncKeyState(VK_TAB) & 0x8000 ? true : false)
        {
            if(!gameInstance.isProjectileReadyToLunch())
            {
                ProjectileState ps(gameInstance.getProjectileType(0),
                                   gameInstance.getBackground(),
                                   gameInstance.getWormsOnTurn().getX() + 15 - 30,
                                   gameInstance.getWormsOnTurn().getY() + 10 - 40 - 5,
                                   (gameInstance.getWormsOnTurn().getDirection() ? 1 : -1) * 10, -10);
                gameInstance.setNewProjectileToReadyState(ps);
            }
        }

        if(GetAsyncKeyState(VK_CAPITAL) & 0x8000 ? true : false)
        {
            if(gameInstance.isProjectileReadyToLunch())
            {
                gameInstance.getProjectileReadyToLaunch().reset();
            }
        }
    }

}

void animate()
{
    bool isLandscapeDestroyed = false;

    gameInstance.getProjectileList().animate();
    std::list<ProjectileState> projectileToDestroyList;

    { //add worms location to bitmap
        HDC hdc = GetDC(NULL);
        HDC srcHDC = CreateCompatibleDC(hdc);
        HDC dstHDC = CreateCompatibleDC(hdc);

        HBITMAP oldSrcBITMAP = (HBITMAP)SelectObject(srcHDC, gameInstance.getLandscape().getLandspaceBitmask());

        //std::cout << "BG: " << gameInstance.getBackground().getWidth() << " " << gameInstance.getBackground().getHeight() << std::endl;

        HBITMAP newDstBITMAP = CreateCompatibleBitmap(hdc, gameInstance.getBackground().getWidth(), gameInstance.getBackground().getHeight());
        HBITMAP oldDstBITMAP = (HBITMAP)SelectObject(dstHDC, newDstBITMAP);

        ReleaseDC(NULL, hdc);

        BitBlt(dstHDC, 0, 0, gameInstance.getBackground().getWidth(), gameInstance.getBackground().getHeight(), srcHDC, 0, 0, SRCCOPY);




        for(auto i = 0u ; i < gameInstance.getWormsList().size() ; ++i)
        {
            //std::cout << "Worms on turn: " << gameInstance.getWormsTurn() << std::endl;
            if(gameInstance.getWormsTurn() == i)
            {
                    std::cout << "Skip: " << gameInstance.getWorms(i).getUserNickname() << std::endl;
                    continue;
            }

            if(gameInstance.getWormsTurn() != i && gameInstance.getWormsList().at(i).getHealth() > 0.0)
            {
              //  std::cout << "Worms blit: " << i << std::endl;
              std::cout << "Location: " << gameInstance.getWormsList().at(i).getX() - 30 << " , " << gameInstance.getWormsList().at(i).getY() - 40 << std::endl;
                gameInstance.getWormsType(0).draw(dstHDC,
                                                  gameInstance.getWormsList().at(i).getX() - 30, gameInstance.getWormsList().at(i).getY() - 40,
                                                  count/5,
                                                  gameInstance.getWormsList().at(i).getDirection(),
                                                  gameInstance.getWormsList().at(i).getDynamicAnimationCounter());
            }
        }

        SelectObject(dstHDC, oldDstBITMAP);
        SelectObject(srcHDC, oldSrcBITMAP);
        DeleteDC(srcHDC);
        DeleteDC(dstHDC);

        //gameInstance.getProjectileList().registerTargetBitmap(gameInstance.getLandscape().getLandspaceBitmask());
        gameInstance.getProjectileList().registerTargetBitmap(newDstBITMAP);
        projectileToDestroyList = gameInstance.getProjectileList().checkTargetHit();
        //gameInstance.getProjectileList().registerTargetBitmap(gameInstance.getLandscape().getLandspaceBitmask());

        DeleteObject(newDstBITMAP);
    }

    std::vector<std::tuple<POINT,double>> explosionPoints;

    for(auto& elem : projectileToDestroyList)
    {
        POINT explosionPoint = elem.getTopOfRocketLocation();
        gameInstance.getExplosionList().addExplosion(std::move(Explosion(explosionPoint.x, explosionPoint.y, elem.getExplosionRadius())));
        gameInstance.getLandscape().destroyLandscape(explosionPoint, elem.getExplosionRadius());
        isLandscapeDestroyed = true;

        explosionPoints.push_back(std::tuple<POINT,double>(explosionPoint, elem.getExplosionRadius()));
    }

    gameInstance.getExplosionList().animate();
    gameInstance.getExplosionList().impact(gameInstance.getWormsList());

    if(isLandscapeDestroyed)
        gameInstance.calculateLandscapeLineList(gameInstance.getLandscape().getLandspaceBitmask());

    for(auto& explosion : explosionPoints)
        for(auto& elem : gameInstance.getWormsList())
        {
            std::cout << "-------------------------------------" << std::endl;
            std::cout << "check worms free fall after explosion" << std::endl;
            std::cout << "Username: " << elem.getUserNickname() << std::endl;
            auto dx = elem.getCurrentPosition().x - std::get<0>(explosion).x;
            auto dy = elem.getCurrentPosition().y - std::get<0>(explosion).y;
            std::cout << "dx: " << dx << " dy: " << dy << std::endl;

            if((dx*dx + dy*dy) <= std::get<1>(explosion)*std::get<1>(explosion) + 10)
            {
                auto res = getFreeFallEndPoint(gameInstance.getLandscapeLineList(), gameInstance.getLandscape().getLandspaceBitmask(), elem.getCurrentPosition());
                if(std::get<0>(res))
                {
                    std::cout << "Free Fall Added" << std::endl;
                    gameInstance.getFreeFallList().push_back(FreeFallState(elem.getCurrentPosition(), std::get<1>(res), elem));
                }
            }
        }


    for(auto it = std::begin(gameInstance.getFreeFallList()); it != std::end(gameInstance.getFreeFallList()); ++it)
        if(it->isFinished())
            it = gameInstance.getFreeFallList().erase(it);

     for(auto& elem : gameInstance.getFreeFallList())
        elem.animate();


    while(gameInstance.getProjectileList().getProjectileCount() == 0u && gameInstance.isNextTurnWithDelaySet())
    {
        gameInstance.nextTurn();
        if(gameInstance.getWormsOnTurn().getHealth() != 0.0)
        {
            gameInstance.resetNextTurnDelayed();
            break;
        }
    }

    while(true)
    {
        if(gameInstance.getWormsOnTurn().getHealth() != 0.0)
        {
            break;
        }
        else
        {
            gameInstance.getProjectileReadyToLaunch().reset();
        }

        gameInstance.nextTurn();

    }
    //std::cout << "newCurrentTurn: " << gameInstance.getWormsTurn() << std::endl;

    auto explosionToDestoryList = gameInstance.getExplosionList().clean();


}



void draw(HWND hwnd){
  ++count;

  HDC hdc = GetDC(hwnd);

  HDC bufferHDC = CreateCompatibleDC(hdc);
  HBITMAP bufferBITMAP = CreateCompatibleBitmap(hdc, gameInstance.getBackground().getWidth(), gameInstance.getBackground().getHeight());
  HBITMAP oldBITMAP = (HBITMAP)SelectObject(bufferHDC, bufferBITMAP);

  gameInstance.getBackground().draw(bufferHDC);
  gameInstance.getLandscape().draw(bufferHDC);


   gameInstance.getProjectileList().draw(bufferHDC);
   gameInstance.getExplosionList().draw(bufferHDC);

   if(gameInstance.isProjectileReadyToLunch())
      gameInstance.getProjectileReadyToLaunch()->render(bufferHDC);





    COLORREF oldTextColor = SetTextColor(bufferHDC, gameInstanceOption()? RGB(0, 0, 0) : RGB(255, 255, 255));

    for(auto& elem : gameInstance.getWormsList())
        if(elem.getHealth() > 0.0)
        {
            gameInstance.getWormsType(0).draw(bufferHDC, elem.getX() - 30, elem.getY() - 40, count/5, elem.getDirection(), elem.getDynamicAnimationCounter());
            SetBkMode(bufferHDC, TRANSPARENT);

            SIZE dim;
            GetTextExtentPoint32(bufferHDC, elem.getUserNickname().c_str(), elem.getUserNickname().size(), &dim);
            TextOut(bufferHDC,
                    elem.getX() + gameInstance.getWormsType(0).getWidth()/2  - dim.cx/2 - 30,
                    elem.getY() - 10 - 40,
                    elem.getUserNickname().c_str(),
                    elem.getUserNickname().size());
            //std::cout << "Username: " << elem.getUserNickname() << std::endl;
         }

    SetTextColor(bufferHDC, oldTextColor);





    auto segmentSize = gameInstance.getBackground().getWidth()  /gameInstance.getWormsList().size();
    for(auto i = 0u ; i < gameInstance.getWormsList().size() ; ++i)
        HealthBar::draw(bufferHDC,
                        segmentSize/10 + i*segmentSize,
                        50,
                        segmentSize*8/10,
                        30,
                        RGB(200, 20, 20),
                        gameInstance.getWormsList().at(i).getHealth(),
                        gameInstance.getWormsList().at(i).getUserNickname());


    if(gameInstance.isProjectileReadyToLunch())
    {
        ProjectileRangeBar::draw(bufferHDC,
                                 gameInstance.getWormsOnTurn().getX() - gameInstance.getWormsType(0).getWidth()/2,
                                 gameInstance.getWormsOnTurn().getY() - gameInstance.getWormsType(0).getHeight()*0.55,
                                 gameInstance.getWormsType(0).getWidth(), 10,
                                 RGB(20, 20, 200),
                                 gameInstance.getProjectileReadyToLaunch()->getRange(),
                                 gameInstance.getProjectileReadyToLaunch()->getMinimumRange(),
                                 gameInstance.getProjectileReadyToLaunch()->getMaximumRange());
    }


    auto gameFinishedResult = gameInstance.isGameFinished();
    if(std::get<0>(gameFinishedResult))
    {
        GameWinnerAnimation::draw(bufferHDC, 0, 0, gameInstance.getBackground().getWidth(), gameInstance.getBackground().getHeight(), std::get<1>(gameFinishedResult).getUserNickname());
    }


  BitBlt(hdc, 0, 0, gameInstance.getBackground().getWidth(), gameInstance.getBackground().getHeight(), bufferHDC, 0, 0, SRCCOPY);

  SelectObject(bufferHDC, oldBITMAP);
  DeleteObject(bufferBITMAP);
  DeleteDC(bufferHDC);

  ReleaseDC(hwnd, hdc);
}

