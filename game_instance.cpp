#include "game_instance.h"


std::tuple<bool,WormsState&> GameInstance::isGameFinished()
{
    std::size_t countAliveWorms = 0u;
    std::size_t idxAliveWorm;

    for(auto i = 0u; i < wormsList.size(); ++i)
        if(wormsList.at(i).getHealth())
        {
            idxAliveWorm = i;
            ++countAliveWorms;
        }

    if(countAliveWorms == 1u)
        return std::tuple<bool,WormsState&>{true, wormsList.at(idxAliveWorm)};
    else
        return std::tuple<bool,WormsState&>(false, wormsList.at(0));
}
