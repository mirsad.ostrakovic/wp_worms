#ifndef FREE_FALL_HPP_INCLUDED
#define FREE_FALL_HPP_INCLUDED

#include <stdexcept>
#include <windows.h>
#include "vector2D.hpp"
#include "worms.hpp"

class FreeFallState{
    public:
        FreeFallState(POINT cp, POINT ep, WormsState& ws);

        void animate();
        bool isFinished();
        POINT getCurrentPosition() const { return currentPosition.getPOINT(); }
        POINT getEndPosition() const { return endPosition.getPOINT(); }

        ~FreeFallState() { worm.resetFreeFalling(); }

    private:
        Vector2D velocity;
        Vector2D currentPosition;
        Vector2D endPosition;
        WormsState& worm;

        const static double airResistance;
        const static Vector2D gravitationalAcceleration;
};

#endif // FREE_FALL_HPP_INCLUDED
