#ifndef GAME_WINNER_HPP_INCLUDED
#define GAME_WINNER_HPP_INCLUDED

#include <windows.h>
#include <wingdi.h>
#include <string>
#include <stdexcept>

class Crown{
    public:
        Crown(const std::string& crownBitmaskBlack,
              const std::string& crownBitmaskWhite,
              int crownWidth, int crownHeight);

        int getWidth() const { return crownBitmaskInfo.bmWidth; }
        int getHeight() const { return crownBitmaskInfo.bmHeight; }

        void draw(HDC bufferHDC, std::size_t xPos, std::size_t yPos);

    private:
        HBITMAP crownBlackBitmap;
        HBITMAP crownWhiteBitmap;
        BITMAP  crownBitmaskInfo;

};



class GameWinnerAnimation{
    public:
        static void draw(HDC bufferHDC, int xPos, int yPos, int width, int height, const std::string& userNickname);
        static Crown crown;
};




#endif // GAME_WINNER_HPP_INCLUDED
