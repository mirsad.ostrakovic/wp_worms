#include "landscape_line.hpp"

POINT LandscapeLine::getLastPoint() const
{
    if(points.size() == 0u)
        return {0, 0};
    return {startCoordinateX + points.size() - 1, points.at(points.size()-1)};
}


std::ostream& operator<<(std::ostream& out, const LandscapeLine& line)
{
    out << "Line start at: " << line.startCoordinateX << std::endl;
    out << "Points: " << std::endl;
    for(auto i = 0u; i < line.points.size(); ++i)
        out << "\t" << line.startCoordinateX + i << "," << line.points.at(i) << std::endl;
    out << "-----------------------------------" << std::endl;

    return out;
}




void addPointToLine(std::vector<LandscapeLine>& lineList, const std::vector<POINT>& pointList)
{
         auto i = 0u;
         double distance;
         int nearestIndex;

         //std::tuple<int, POINT, double> tuplic;
         std::vector<std::tuple<int,POINT,double>> nearestIndexList;

         //std::cout << "---------------------------------------" << std::endl;
         //std::cout << "---------------------------------------" << std::endl;
         //std::cout << "Number of lines: " << lineList.size() << std::endl;
         //std::cout << "----------------------------------------" << std::endl;


         for(const auto& elem : pointList) // find for every point the most appropriate line to add
         {
             nearestIndex = -1;

             for(auto i = 0u; i < lineList.size(); ++i)
                if(lineList.at(i).isActive(elem.x))
                {
              //      std::cout << "Have active line on x: " << elem.x  << "y: " << elem.y << " index: " << i << std::endl;
                    nearestIndex = i;
                    distance = abs(lineList.at(i).getLastPoint().y - elem.y);
                    break;
                }


            //std::cout << "nearestIndex: " << nearestIndex << " lineList.size(): " << lineList.size() << std::endl;

             for(auto i = nearestIndex + 1 ; i < lineList.size() ; ++i)
             {
                //std::cout << "check other lines" << std::endl;
                if(lineList.at(i).isActive(elem.x))
                {
                    double calcDistance = abs(lineList.at(i).getLastPoint().y - elem.y);
                    if(calcDistance < distance)
                    {
                        //std::cout << "New active line - Change nearest index from: " << nearestIndex << " to: ";
                        distance = calcDistance;
                        nearestIndex = i;
                        //std::cout << nearestIndex << std::endl;
                    }
                }
             }

             //std::cout << "Nearest index: " << nearestIndex  << " distance: " << distance << std::endl;
             nearestIndexList.push_back(std::tuple<int,POINT,double>{nearestIndex, elem, distance});
         }

    try{

         bool alreadyAdded;

         //std::cout << "---------------------------------------" << std::endl;
         //std::cout << "POINT to process: " << nearestIndexList.size() << std::endl;
         //std::cout << "Number of lines: " << lineList.size() << std::endl;
         //std::cout << "----------------------------------------" << std::endl;

         for(auto i = 0u; i < nearestIndexList.size(); ++i)
         {
            if(std::get<0>(nearestIndexList.at(i)) == -1) // ako nema najblizeg indexa dodaj tacku kao pocetak nove linije
            {
                lineList.push_back(std::move(LandscapeLine(std::get<1>(nearestIndexList.at(i)))));
                continue;
            }


            alreadyAdded = false;

            for(auto j = i + 1 ; j < nearestIndexList.size(); ++j)
            {
                if(std::get<0>(nearestIndexList.at(i)) == std::get<0>(nearestIndexList.at(j)))
                {
                    if(std::get<2>(nearestIndexList.at(i)) < std::get<2>(nearestIndexList.at(j)))  // distance(i) < distance(j)
                    {
                        //found point with same line to add, but have larger distance -> allocate line for it
                        //std::cout << "NEW LINE start at: (" << std::get<1>(nearestIndexList.at(j)).x << ", " << std::get<1>(nearestIndexList.at(j)).y << std::endl;
                        lineList.push_back(std::move(LandscapeLine(std::get<1>(nearestIndexList.at(j)))));
                        nearestIndexList.erase(std::begin(nearestIndexList) + j); // delete it from line to proces list
                        if(i + 1 != j)
                            j--;
                        //std::cout << "For the same index: " << std::get<0>(nearestIndexList.at(i)) << " ";
                        //std::cout << "POINT(" << i << ") has SMALLEr distance than POINT(" << j << ")" << std::endl;
                        continue;
                    }
                    else
                    {
                        //std::cout << "For the same index: " << std::get<0>(nearestIndexList.at(i)) << " ";
                        //std::cout << "POINT(" << i << ") has LARGER distance than POINT(" << j << ")" << std::endl;
                        lineList.push_back(std::move(LandscapeLine(std::get<1>(nearestIndexList.at(i)))));
                        alreadyAdded = true;
                        break;
                    }
                }
            }

            if(!alreadyAdded)
            {
                //std::cout << "Add POINT(" << std::get<1>(nearestIndexList.at(i)).x << ", " << std::get<1>(nearestIndexList.at(i)).y << ") to line with index " << std::get<0>(nearestIndexList.at(i)) << std::endl;
                (lineList.at(std::get<0>(nearestIndexList.at(i)))).addPoint((std::get<1>(nearestIndexList.at(i))).y);
            }
         }


      }
      catch(...)
      {
        std::cerr << "Error in adding data" << std::endl;
        exit(-2);
      }
}





std::vector<LandscapeLine> getLandspaceMovingLines(HBITMAP landscapeBITMAP)
{
    /*
      BITMAP bitmapInfo;
      GetObject(landscapeBITMAP, sizeof(bitmapInfo), &bitmapInfo);
      HDC hdc = GetDC(NULL);
      HDC srcHDC = CreateCompatibleDC(hdc);
      ReleaseDC(NULL, hdc);
      HBITMAP oldSrcBITMAP = (HBITMAP)SelectObject(srcHDC, landscapeBITMAP);
    */

      //--------------------------------------------------------------------
      //--------------------------------------------------------------------
      //--------------------------------------------------------------------
    BITMAP bitmapInfo;
    GetObject(landscapeBITMAP, sizeof(bitmapInfo), &bitmapInfo);

    HDC hdc = GetDC(NULL);
    HDC sourceHDC = CreateCompatibleDC(hdc);
    ReleaseDC(NULL, hdc);
    HBITMAP oldSourcBITMAP = (HBITMAP)SelectObject(sourceHDC, landscapeBITMAP);


    BYTE* lpPixelsSrc;

    BITMAPINFO MyBMInfoSrc = {0};
    {
            MyBMInfoSrc.bmiHeader.biSize = sizeof(MyBMInfoSrc.bmiHeader);

            if(0 == GetDIBits(sourceHDC, landscapeBITMAP, 0, 0, NULL, &MyBMInfoSrc, DIB_RGB_COLORS))
            {
                // error handling
            }

            lpPixelsSrc = new BYTE[MyBMInfoSrc.bmiHeader.biSizeImage];

            MyBMInfoSrc.bmiHeader.biBitCount = 32;
            MyBMInfoSrc.bmiHeader.biCompression = BI_RGB;

            MyBMInfoSrc.bmiHeader.biHeight = abs(MyBMInfoSrc.bmiHeader.biHeight);

            if(0 == GetDIBits(sourceHDC, landscapeBITMAP, 0, MyBMInfoSrc.bmiHeader.biHeight,
                              lpPixelsSrc, &MyBMInfoSrc, DIB_RGB_COLORS))
            {
                // error handling
            }
    }

  //--------------------------------------------------------------------
  //--------------------------------------------------------------------
  //--------------------------------------------------------------------



    int transitionCount;
    //int pixel1, pixel2;
    std::vector<POINT> scanLinePoints;
    std::vector<LandscapeLine> lineList;


    /*
      for(int i = 0; i < bitmapInfo.bmWidth; ++i)
      {
        transitionCount = 0;
        scanLinePoints = std::vector<POINT>();

        for(int j = 0; j < bitmapInfo.bmHeight - 1; ++j)
        {
            pixel1 = GetPixel(srcHDC, i, j);
            pixel2 = GetPixel(srcHDC, i, j+1);

            if((pixel1 == RGB(0, 0, 0) && pixel2 == RGB(255, 255, 255)) ||
               (pixel1 == RGB(255, 255, 255) && pixel2 == RGB(0, 0, 0))    )
            {

                ++transitionCount;
                if(transitionCount%2)
                {
                    POINT pt{i,j};
                    scanLinePoints.push_back(pt);
                }
            }
        }

        addPointToLine(lineList, scanLinePoints);
      }
    */
      //================================================================
      //================================================================

      std::size_t x, y;

      for ( x = 0; x < MyBMInfoSrc.bmiHeader.biWidth; x++ )
      {
        transitionCount = 0;
        scanLinePoints = std::vector<POINT>();

        for ( y = 1; y < MyBMInfoSrc.bmiHeader.biHeight - 1; y++ )
        {
            int srcIndex = 4*(x + MyBMInfoSrc.bmiHeader.biWidth*(MyBMInfoSrc.bmiHeader.biHeight - y -1));
            int srcNextIndex = 4*(x + MyBMInfoSrc.bmiHeader.biWidth*(MyBMInfoSrc.bmiHeader.biHeight - y));

            if(srcIndex < 0 || srcIndex  >= (MyBMInfoSrc.bmiHeader.biSizeImage - 2))
            {
                std::cerr << "Invalid srcIndex: " << srcIndex << std::endl;
                exit(-1);
            }

            if(srcNextIndex < 0 || srcNextIndex  >= (MyBMInfoSrc.bmiHeader.biSizeImage - 2))
            {
                std::cerr << "Invalid srcNextIndex: " << srcNextIndex  << std::endl;
                std::cout << "ImgSize: " << MyBMInfoSrc.bmiHeader.biSizeImage << std::endl;
                exit(-1);
            }



            if((lpPixelsSrc[srcIndex] == 0 && lpPixelsSrc[srcNextIndex] == 255) ||
               (lpPixelsSrc[srcIndex] == 255 && lpPixelsSrc[srcNextIndex] == 0)   )// if R is 0 then and B and G is 0, same for R = 255
            {
                 ++transitionCount;
                if(transitionCount%2)
                {
                    POINT pt{x,y-1};
                    scanLinePoints.push_back(pt);
                }
            }
        }

        if(transitionCount == 0u) // if landscape point was not find, then add bottom point of landscape as line point
        {
            POINT pt;
            pt.x = x;
            pt.y = y;

            scanLinePoints.push_back(pt);
        }

        addPointToLine(lineList, scanLinePoints);
      }

      //================================================================
      //================================================================

    /*

      SelectObject(srcHDC, oldSrcBITMAP);
      DeleteDC(srcHDC);
    */

    delete []lpPixelsSrc;
    SelectObject(sourceHDC, oldSourcBITMAP);
    DeleteDC(sourceHDC);
    return lineList;
}

std::tuple<int,POINT> LandscapeLine::getNextPoint(POINT pt, bool direction)
{
    POINT newPoint;

    std::cout << "pt.x = " << pt.x << " , pt.y = " << pt.y << " , startCoordinateX + points.size() = " << startCoordinateX + points.size() << std::endl;
    std::cout << "pt.x - startCoordinateX: " << pt.x - startCoordinateX << " , " << "points.size() = " << points.size() << std::endl;
    std::cout << "startCoordinateX: " << startCoordinateX << std::endl;
    std::cout << "Direction: " << direction << std::endl;

    if(pt.x < startCoordinateX || pt.x >= startCoordinateX + points.size())
    {
        std::cout << "pt.x = " << pt.x << ", startCoordinateX = " << startCoordinateX << ", startCoordinateX + points.size() = " << startCoordinateX + points.size() << std::endl;
        return std::tuple<int,POINT>{0, POINT()};
    }
    else if(pt.x == startCoordinateX && !direction) // endpoints
    {
        if(pt.y != points.at(pt.x - startCoordinateX))
        {
            std::cout << "Y DONT MATCH 1 - pt.y = " << pt.y << ", ppoints.at(pt.x - startCoordinateX) = " << points.at(pt.x - startCoordinateX) << std::endl;
            return std::tuple<int,POINT>{0, POINT()};
        }

        std::cout << "left endpoint" << std::endl;
        return std::tuple<int,POINT>{1, pt};
    }
    else if(pt.x == (startCoordinateX + points.size() - 1) && direction) // endpoint
    {
        if(pt.y != points.at(pt.x - startCoordinateX))
        {
            std::cout << "Y DONT MATCH 2 - pt.y = " << pt.y << ", ppoints.at(pt.x - startCoordinateX) = " << points.at(pt.x - startCoordinateX) << std::endl;
            return std::tuple<int,POINT>{0, POINT()};
        }

        std::cout << "right endpoint" << std::endl;
        return std::tuple<int,POINT>{1, pt};
    }
    else
    {
        if( (pt.y - points.at(pt.x - startCoordinateX))*(pt.y - points.at(pt.x - startCoordinateX)) > 100 )
        {
            std::cout << "Y DONT MATCH 3 - pt.y = " << pt.y << ", ppoints.at(pt.x - startCoordinateX) = " << points.at(pt.x - startCoordinateX) << std::endl;
            return std::tuple<int,POINT>{0, POINT()};
        }

        if(direction)
        {
            newPoint.x = pt.x + 1;
            newPoint.y = points.at(pt.x - startCoordinateX + 1);
            return std::tuple<int,POINT>{2, newPoint};
        }
        else
        {
            newPoint.x = pt.x - 1;
            newPoint.y = points.at(pt.x - startCoordinateX - 1);
            return std::tuple<int,POINT>{2, newPoint};
        }
    }
}


std::tuple<int,POINT> moveLeft(std::vector<LandscapeLine>& landscapeLine, Landscape& landscape, POINT currentPoint)
{

    for(auto& elem : landscapeLine)
    {
        auto result = elem.getNextPoint(currentPoint, false);

        if(std::get<0>(result) == 2) // point is in middle of line
        {
            return std::tuple<int,POINT>{2, std::get<1>(result)};
        }
        else if(std::get<0>(result) == 1) // endpoint of moving line
        {
            std::cout << "ENDPOINT" << std::endl;
            auto freeFallStartPoint = currentPoint;
            freeFallStartPoint.x--;

            auto result2 = getFreeFallEndPoint(landscapeLine, landscape.getLandspaceBitmask(), freeFallStartPoint);
            std::cout << "ENDPOINT2" << std::endl;
            if(std::get<0>(result2))
            {
                std::cout << "New point after free fall, x = " << std::get<1>(result2).x << " , y = " << std::get<1>(result2).y << std::endl;
                return std::tuple<int,POINT>{1, std::get<1>(result2)};
            }
            else
            {
                std::cout << "Dont find free fall point" << std::endl;
                return std::tuple<int,POINT>{2, currentPoint};
            }
        }
    }

    return std::tuple<int,POINT>{0, POINT()};

    //throw std::string("moveLeft(): dont find next point");
    /*
    auto result2 = getFreeFallEndPoint(landscapeLine, currentPoint);
    if(std::get<0>(result2))
    {
        std::cout << "New point after free fall, x = " << std::get<1>(result2).x << " , y = " << std::get<1>(result2).y << std::endl;
        return std::get<1>(result2);
    }
    else
        return currentPoint;
    */
}

std::tuple<int,POINT> moveRight(std::vector<LandscapeLine>& landscapeLine, Landscape& landscape, POINT currentPoint)
{

    for(auto& elem : landscapeLine)
    {
        auto result = elem.getNextPoint(currentPoint, true);

        if(std::get<0>(result) == 2) // point is in middle of line
        {
            return std::tuple<int,POINT>{2, std::get<1>(result)};
        }
        else if(std::get<0>(result) == 1) // endpoint of moving line
        {
            std::cout << "ENDPOINT" << std::endl;

            auto freeFallStartPoint = currentPoint;
            freeFallStartPoint.x++;

            auto result2 = getFreeFallEndPoint(landscapeLine, landscape.getLandspaceBitmask(), freeFallStartPoint);
            std::cout << "ENDPOINT2" << std::endl;
            if(std::get<0>(result2))
            {
                std::cout << "New point after free fall, x = " << std::get<1>(result2).x << " , y = " << std::get<1>(result2).y << std::endl;
                return std::tuple<int,POINT>{1, std::get<1>(result2)};
            }
            else
            {
                std::cout << "Dont find free fall point" << std::endl;
                return std::tuple<int,POINT>{2, currentPoint};
            }
        }
    }

    return std::tuple<int,POINT>{0, POINT()};



    /*
    for(auto& elem : landscapeLine)
    {
        auto result = elem.getNextPoint(currentPoint, true);

        if(std::get<0>(result) == 2) // point is in middle of line
        {
            return std::get<1>(result);
        }
        else if(std::get<0>(result) == 1) // endpoint of moving line
        {
            auto result2 = getFreeFallEndPoint(landscapeLine, currentPoint);
            if(std::get<0>(result2))
            {
                std::cout << "New point after free fall, x = " << std::get<1>(result2).x << " , y = " << std::get<1>(result2).y << std::endl;
                return std::get<1>(result2);
            }
            else
            {
                std::cout << "Dont find free fall point" << std::endl;
                return currentPoint;
            }
        }
    }

    throw std::string("moveRight(): dont find next point");
    */
}



std::tuple<bool,POINT> LandscapeLine::getPointLocation(std::size_t xPos)
{
    POINT newPoint;

    if(xPos < startCoordinateX || xPos >= startCoordinateX + points.size())
    {
        //std::cout << "pt.x = " << pt.x << ", startCoordinateX = " << startCoordinateX << ", startCoordinateX + points.size() = " << startCoordinateX + points.size() << std::endl;
        return std::tuple<bool,POINT>{false, POINT()};
    }
    else
    {
        newPoint.x = xPos;
        newPoint.y = points.at(xPos - startCoordinateX);
        return std::tuple<bool,POINT>{true, newPoint};
    }
}



static bool isObstacleExists(HBITMAP landscapeBITMAP, POINT startPOINT, POINT endPOINT)
{
    if(startPOINT.x != endPOINT.x || endPOINT.y <= startPOINT.y)
        throw std::invalid_argument("isObstacleExists(): invalid startPOINT and endPOINT");

    BITMAP bitmapInfo;
    GetObject(landscapeBITMAP, sizeof(bitmapInfo), &bitmapInfo);

    HDC hdc = GetDC(NULL);
    HDC srcHDC = CreateCompatibleDC(hdc);

    ReleaseDC(NULL, hdc);

    HBITMAP oldSrcBITMAP = (HBITMAP)SelectObject(srcHDC, landscapeBITMAP);

    bool returnValue = false;

    for(int j = startPOINT.y; j < endPOINT.y; ++j)
    {
        if((GetPixel(srcHDC, startPOINT.x, j) != RGB(0, 0, 0)))
        {
            //std::cout << "startPOINT: " << startPOINT.x << " , " << startPOINT.y << " ";
            //std::cout << "endPOINT: " << endPOINT.x << " , " << endPOINT.y << " ";
            //std::cout << "currentPOINT: " << endPOINT.x << " , " << j << " ";

            returnValue = true;
            break;
        }
        /*
        auto color = GetPixel(srcHDC, startPOINT.x, j);
        int red = GetRValue(color);
        int green = GetGValue(color);
        int blue = GetBValue(color);

        std::cout << "currentPOINT: " << endPOINT.x << " , " << j << "  -  RGB: ";
        std::cout << "R: " << red << " G: " << green << " B: " << blue << std::endl;
        */
    }


    SelectObject(srcHDC, oldSrcBITMAP);
    DeleteDC(srcHDC);

    return returnValue;
}


std::tuple<bool,POINT> getFreeFallEndPoint(std::vector<LandscapeLine>& landscapeLine, HBITMAP landscapeBITMAP, POINT currentPoint)
{
    std::vector<POINT> possibleFreeFallPoints;

    for(auto& elem : landscapeLine)
    {
        auto result = elem.getPointLocation(currentPoint.x);
        if(std::get<0>(result))
        {
            if(((POINT)(std::get<1>(result))).y > currentPoint.y)
            {

                std::cout << "startCoordinateX: " << elem.getStartX() << "freeFallPoint.y: " << std::get<1>(result).y << " , currentPoint: " << currentPoint.y << std::endl;
                possibleFreeFallPoints.push_back(std::get<1>(result));
            }
        }
    }

    if(possibleFreeFallPoints.size() == 0u)
        return std::tuple<bool,POINT>{false, POINT()};

    POINT returnPoint = possibleFreeFallPoints.at(0);

    for(auto i = 1u; i < possibleFreeFallPoints.size(); ++i)
        if(possibleFreeFallPoints.at(i).y < returnPoint.y)
            returnPoint = possibleFreeFallPoints.at(i);

    if(isObstacleExists(landscapeBITMAP, currentPoint, returnPoint))
    {
        std::cout << "OBSTACLE EXISTS" << std::endl;
        return std::tuple<bool,POINT>{false, POINT()};
    }


    return std::tuple<bool,POINT>{true, returnPoint};
}



POINT findWormsTopLocation(HBITMAP landscapeBITMAP, int xPos)
{
    BITMAP bitmapInfo;
    GetObject(landscapeBITMAP, sizeof(bitmapInfo), &bitmapInfo);

    HDC hdc = GetDC(NULL);
    HDC srcHDC = CreateCompatibleDC(hdc);

    ReleaseDC(NULL, hdc);

    HBITMAP oldSrcBITMAP = (HBITMAP)SelectObject(srcHDC, landscapeBITMAP);


    int pixel1, pixel2;

    for(int j = 0; j < bitmapInfo.bmHeight - 1; ++j)
    {
            pixel1 = GetPixel(srcHDC, xPos, j);
            pixel2 = GetPixel(srcHDC, xPos, j+1);

            if((pixel1 == RGB(0, 0, 0) && pixel2 == RGB(255, 255, 255)) ||
               (pixel1 == RGB(255, 255, 255) && pixel2 == RGB(0, 0, 0))    )
            {
                POINT pt;
                pt.x = xPos;
                pt.y = j;

                SelectObject(srcHDC, oldSrcBITMAP);
                DeleteDC(srcHDC);

                return pt;
            }

    }


    SelectObject(srcHDC, oldSrcBITMAP);
    DeleteDC(srcHDC);

    POINT pt;
    pt.x = -1;
    pt.y = -1;

    return pt;
}
