#include "game_winner.hpp"


Crown GameWinnerAnimation::crown("crown_black.bmp", "crown_white.bmp", 250, 275);

void GameWinnerAnimation::draw(HDC bufferHDC, int xPos, int yPos, int width, int height, const std::string& userNickname)
{
  static  std::size_t animationCounter = 1u;
  static int counterDirection = 1;

  crown.draw(bufferHDC, xPos + width/2, yPos + height/2);



  COLORREF oldTextColor = SetTextColor(bufferHDC, RGB(50 + animationCounter % 180, animationCounter % 180, animationCounter % 180));
  HFONT font = CreateFont(15, 8, 0, 0, 500, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_OUTLINE_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, VARIABLE_PITCH, TEXT("Arial"));
  HFONT oldFont = (HFONT)SelectObject(bufferHDC, font);

  //display user nickname
  SIZE dim;
  GetTextExtentPoint32(bufferHDC, userNickname.c_str(), userNickname.size(), &dim);

  TextOut(bufferHDC,
          xPos + width/2  - dim.cx/2,
          yPos + height*6/10 - dim.cy/2,
          userNickname.c_str(),
          userNickname.size());




  SelectObject(bufferHDC, oldFont);
  DeleteObject(font);

  SetTextColor(bufferHDC, oldTextColor);

  animationCounter = animationCounter + counterDirection;

  if(animationCounter == 0u || animationCounter == 179u)
    counterDirection = -counterDirection;
}






Crown::Crown(const std::string& crownBitmaskBlackFilename,
             const std::string& crownBitmaskWhiteFilename,
             int crownWidth, int crownHeight):
    crownWhiteBitmap{ (HBITMAP)LoadImage(NULL, crownBitmaskWhiteFilename.c_str(), IMAGE_BITMAP, crownWidth, crownHeight, LR_LOADFROMFILE) },
    crownBlackBitmap{ (HBITMAP)LoadImage(NULL, crownBitmaskBlackFilename.c_str(), IMAGE_BITMAP, crownWidth, crownHeight, LR_LOADFROMFILE) }
{
    if( crownWhiteBitmap == NULL || crownBlackBitmap == NULL )
        throw std::invalid_argument("Crown(): invalid  crownBitmaskFilename");

    GetObject(crownWhiteBitmap, sizeof(crownBitmaskInfo), &crownBitmaskInfo);
}

void Crown::draw(HDC bufferHDC, std::size_t xPos, std::size_t yPos)
{
    HDC tmpHDC = CreateCompatibleDC(bufferHDC);
    HBITMAP oldTmpBITMAP = (HBITMAP)SelectObject(tmpHDC, crownWhiteBitmap);

    BitBlt(bufferHDC, xPos - getWidth()/2, yPos - getHeight()/2, getWidth(), getHeight(), tmpHDC, 0, 0, SRCAND);

    SelectObject(tmpHDC, crownBlackBitmap);

    BitBlt(bufferHDC, xPos - getWidth()/2, yPos - getHeight()/2, getWidth(), getHeight(), tmpHDC, 0, 0, SRCPAINT);

    SelectObject(tmpHDC, oldTmpBITMAP);
    DeleteDC(tmpHDC);
}

