#if defined(UNICODE) && !defined(_UNICODE)
    #define _UNICODE
#elif defined(_UNICODE) && !defined(UNICODE)
    #define UNICODE
#endif

#include <tchar.h>
#include <windows.h>
#include <mmsystem.h>
#include <chrono>
#include <thread>
#include <ctime>
#include <iostream>
#include "game_loop_functions.hpp"
#include "worms.hpp"



/*  Declare Windows procedure  */
LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);

/*  Make the class name into a global variable  */
TCHAR szClassName[ ] = _T("CodeBlocksWindowsApp");

int WINAPI WinMain (HINSTANCE hThisInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR lpszArgument,
                     int nCmdShow)
{

    RECT clientRect;
    clientRect.left = clientRect.top = 0;
    clientRect.right = gameInstance.getBackground().getWidth();
    clientRect.bottom = gameInstance.getBackground().getHeight();

    AdjustWindowRect(&clientRect, WS_CAPTION, 0);


    HWND hwnd;               /* This is the handle for our window */
    MSG messages;            /* Here messages to the application are saved */
    WNDCLASSEX wincl;        /* Data structure for the windowclass */

    /* The Window structure */
    wincl.hInstance = hThisInstance;
    wincl.lpszClassName = szClassName;
    wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
    wincl.style = CS_DBLCLKS;                 /* Catch double-clicks */
    wincl.cbSize = sizeof (WNDCLASSEX);

    /* Use default icon and mouse-pointer */
    wincl.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
    wincl.lpszMenuName = NULL;                 /* No menu */
    wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
    wincl.cbWndExtra = 0;                      /* structure or the window instance */
    /* Use Windows's default colour as the background of the window */
    wincl.hbrBackground = (HBRUSH) COLOR_BACKGROUND;

    /* Register the window class, and if it fails quit the program */
    if (!RegisterClassEx (&wincl))
        return 0;

    /* The class is registered, let's create the program*/
    hwnd = CreateWindowEx (
           0,                   /* Extended possibilites for variation */
           szClassName,         /* Classname */
           TEXT("WORMS - Mirsad Ostrakovic FET"),       /* Title Text */
           WS_CAPTION, /* default window */
           CW_USEDEFAULT,       /* Windows decides the position */
           CW_USEDEFAULT,       /* where the window ends up on the screen */
           clientRect.right - clientRect.left,                 /* The programs width */
           clientRect.bottom - clientRect.top,                 /* and height in pixels */
           HWND_DESKTOP,        /* The window is a child-window to desktop */
           NULL,                /* No menu */
           hThisInstance,       /* Program Instance handler */
           NULL                 /* No Window Creation data */
           );

    std::vector<std::string> players = {"mirsad.ostrakovic", "RI_bot"};

    initGameInstance(players);

    /* Make the window visible on the screen */
    ShowWindow (hwnd, nCmdShow);

    while(true)
    {
      auto startTime = std::chrono::system_clock::now();

      if(PeekMessage(&messages, NULL, 0, 0, PM_REMOVE))
      {
        if(messages.message == WM_QUIT) break;
        TranslateMessage(&messages);
        DispatchMessage(&messages);
      }

      try{
        updateStates(hwnd);
        animate();
        draw(hwnd);
      }
      catch(...)
      {
          std::cout << "ERROR" << std::endl;
          exit(-1);
      }

      //while(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - startTime) < std::chrono::milliseconds(7))
      //std::this_thread::sleep_for(std::chrono::milliseconds(3));

    }

    /* The program return-value is 0 - The value that PostQuitMessage() gave */
    return messages.wParam;
}


/*  This function is called by the Windows function DispatchMessage()  */

LRESULT CALLBACK WindowProcedure (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)                  /* handle the messages */
    {

        case WM_RBUTTONDOWN:
        {
            if(gameInstance.isProjectileReadyToLunch())
            {
                gameInstance.launchReadyProjectile();

                std::cout << "Projectile created" << std::endl;

                std::cout << "oldCurrentTurn: " << gameInstance.getWormsTurn() << " ";
                gameInstance.setNextTurnDelayed();
                std::cout << "newCurrentTurn: " << gameInstance.getWormsTurn() << std::endl;
            }
        }
        break;

        case WM_DESTROY:
            PostQuitMessage (0);       /* send a WM_QUIT to the message queue */
            break;

        default:                      /* for messages that we don't deal with */
            return DefWindowProc (hwnd, message, wParam, lParam);
    }

    return 0;
}
