#include "landspace.hpp"


Landscape::Landscape(const std::string& landspaceBitmaskBlack,
                              const std::string& landspaceBitmaskWhite,
                              int landspaceWidth, int landscapeHeight,
                              const std::string& texture, int textureWidth, int textureHeight):
    landspaceBitmaskWhiteBitmap{ (HBITMAP)LoadImage(NULL, landspaceBitmaskWhite.c_str(), IMAGE_BITMAP, landspaceWidth, landscapeHeight, LR_LOADFROMFILE) },
    landspaceBitmaskBlackBitmap{ (HBITMAP)LoadImage(NULL, landspaceBitmaskBlack.c_str(), IMAGE_BITMAP, landspaceWidth, landscapeHeight, LR_LOADFROMFILE) },
    textureBitmap{ (HBITMAP)LoadImage(NULL, texture.c_str(), IMAGE_BITMAP, textureWidth, textureHeight, LR_LOADFROMFILE) }
{
    if( landspaceBitmaskWhiteBitmap == NULL || landspaceBitmaskBlackBitmap == NULL )
        throw std::invalid_argument("Landscape(): invalid landspaceBitmaskFilename");

    if( textureBitmap == NULL )
        throw std::invalid_argument("Landscape(): invalid textureFilename");

    GetObject(landspaceBitmaskWhiteBitmap, sizeof(landspaceBitmaskInfo), &landspaceBitmaskInfo);
    GetObject(textureBitmap, sizeof(textureBitmapInfo), &textureBitmapInfo);
}


void Landscape::destroyLandscape(const POINT& point, int radius)
{
    HDC hdc = GetDC(NULL);
    HDC tmpHDC = CreateCompatibleDC(hdc);
    ReleaseDC(NULL, hdc);

   HBITMAP tmpHDCOldBitmap = (HBITMAP)SelectObject(tmpHDC, landspaceBitmaskWhiteBitmap);

   HBRUSH oldBrush = (HBRUSH)SelectObject(tmpHDC , GetStockObject(WHITE_BRUSH));
   HPEN oldPen = (HPEN)SelectObject(tmpHDC, GetStockObject(WHITE_PEN));

   Ellipse(tmpHDC, point.x - radius, point.y - radius, point.x + radius, point.y + radius);

   SelectObject(tmpHDC, landspaceBitmaskBlackBitmap);

   SelectObject(tmpHDC, GetStockObject(BLACK_BRUSH));
   SelectObject(tmpHDC, GetStockObject(BLACK_PEN));

   Ellipse(tmpHDC, point.x - radius, point.y - radius, point.x + radius, point.y + radius);

   SelectObject(tmpHDC, oldBrush);
   SelectObject(tmpHDC, oldPen);
   SelectObject(tmpHDC, tmpHDCOldBitmap);

   DeleteDC(tmpHDC);
}



void Landscape::draw(HDC bufferHDC)
{
    HDC landspaceBitmaskWhiteHDC = CreateCompatibleDC(bufferHDC);
    HBITMAP landspaceWhiteHDCOldBitmap = (HBITMAP)SelectObject(landspaceBitmaskWhiteHDC, landspaceBitmaskWhiteBitmap);

    HDC landspaceBitmaskBlackHDC = CreateCompatibleDC(bufferHDC);
    HBITMAP landspaceBlackHDCOldBitmap = (HBITMAP)SelectObject(landspaceBitmaskBlackHDC, landspaceBitmaskBlackBitmap);

    HDC textureHDC = CreateCompatibleDC(bufferHDC);
    HBITMAP textureHDCOldBitmap = (HBITMAP)SelectObject(textureHDC, textureBitmap);


    for(auto i = 0u ; i <= (getWidth() / getTextureWidth()) ; ++i)
        for(auto j = 0u ; j <= (getHeight() / getTextureHeight()) ; ++j)
        {
            HBITMAP fragmentBitmap = CreateCompatibleBitmap(bufferHDC, getTextureWidth(), getTextureHeight());
            HDC fragmentHDC = CreateCompatibleDC(bufferHDC);
            HBITMAP fragmentHDCOldBitmap = (HBITMAP)SelectObject(fragmentHDC, fragmentBitmap);

            BitBlt(bufferHDC, i * getTextureWidth(), j * getTextureHeight(), getTextureWidth(), getTextureHeight(), landspaceBitmaskWhiteHDC, i * getTextureWidth(), j * getTextureHeight(), SRCAND);

            BitBlt(fragmentHDC, 0, 0, getTextureWidth(), getTextureHeight(), textureHDC, 0, 0, SRCCOPY);
            BitBlt(fragmentHDC, 0, 0, getTextureWidth(), getTextureHeight(), landspaceBitmaskBlackHDC, i * getTextureWidth(), j * getTextureHeight(), SRCAND);

            BitBlt(bufferHDC, i * getTextureWidth(), j * getTextureHeight(), getTextureWidth(), getTextureHeight(), fragmentHDC, 0, 0, SRCPAINT);

            SelectObject(fragmentHDC, fragmentHDCOldBitmap);
            DeleteObject(fragmentBitmap);
            DeleteDC(fragmentHDC);
        }

    SelectObject(textureHDC, textureHDCOldBitmap);
    DeleteDC(textureHDC);

    SelectObject(landspaceBitmaskBlackHDC, landspaceBlackHDCOldBitmap);
    DeleteDC(landspaceBitmaskBlackHDC);

    SelectObject(landspaceBitmaskWhiteHDC, landspaceWhiteHDCOldBitmap);
    DeleteDC(landspaceBitmaskWhiteHDC);
}



