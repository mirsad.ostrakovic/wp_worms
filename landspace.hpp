#ifndef LANDSPACE_HPP_INCLUDED
#define LANDSPACE_HPP_INCLUDED

#include <windows.h>
#include <string>
#include <stdexcept>
#include <iostream>

class Landscape{
    public:
        Landscape(const std::string& landspaceBitmaskBlack,
                  const std::string& landspaceBitmaskWhite,
                  int landspaceWidth, int landscapeHeight,
                  const std::string& texture, int textureWidth, int textureHeight);

        int getWidth() const { return landspaceBitmaskInfo.bmWidth; }
        int getHeight() const { return landspaceBitmaskInfo.bmHeight; }

        HBITMAP getLandspaceBitmask() { return landspaceBitmaskBlackBitmap; }

        void destroyLandscape(const POINT& point, int radius);
        void draw(HDC bufferHDC);

    private:
        HBITMAP landspaceBitmaskBlackBitmap;
        HBITMAP landspaceBitmaskWhiteBitmap;
        BITMAP landspaceBitmaskInfo;
        HBITMAP textureBitmap;
        BITMAP textureBitmapInfo;

        int getTextureWidth() const { return textureBitmapInfo.bmWidth; }
        int getTextureHeight() const { return textureBitmapInfo.bmHeight; }

};


#endif // LANDSPACE_HPP_INCLUDED
