#ifndef PROJECTILE_LIST_HPP_INCLUDED
#define PROJECTILE_LIST_HPP_INCLUDED

#include <list>
#include <windows.h>
#include "projectile.hpp"

class ProjectileList{
    public:
        void registerTargetBitmap(HBITMAP bitmap) { targetBITMAP = bitmap; }
        void addNew(ProjectileState projectile) { projectileList.push_back(projectile); }
        void animate();
        void draw(HDC hdc);
        std::list<ProjectileState> checkTargetHit();

        std::size_t getProjectileCount() const { return projectileList.size(); }

    private:
        HBITMAP targetBITMAP;
        std::list<ProjectileState> projectileList;
};


#endif // PROJECTILE_LIST_HPP_INCLUDED
