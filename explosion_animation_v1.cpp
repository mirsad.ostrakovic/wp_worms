#include "explosion_animation_v1.hpp"


 std::list<Explosion> ExplosionAnimationList::clean()
 {
    std::list<Explosion> finishedExplosionList;

    for(auto it = std::begin(explosionList); it != std::end(explosionList); ++it)
        if(it->isFinished())
        {
            finishedExplosionList.push_back(std::move(*it));
            it = explosionList.erase(it);
        }

    return finishedExplosionList;
 }


void Explosion::draw(HDC bufferHDC)
{
    // R 64 G 64->8 B->4

    if(animationRadius > 0)
    {
        HBRUSH explosionBRUSH = CreateSolidBrush(RGB(200, 8 + (128-8) * animationRadius/radius, 8));
        HBRUSH oldBRUSH = (HBRUSH)SelectObject(bufferHDC, explosionBRUSH);

        Ellipse(bufferHDC, xPos - animationRadius, yPos - animationRadius, xPos + animationRadius, yPos + animationRadius);

        SelectObject(bufferHDC, oldBRUSH);
        DeleteObject(explosionBRUSH);
    }
}

void Explosion::animate()
{
    animationRadius -= 1 + animationRadius * 0.04;
}

void Explosion::impact(std::vector<WormsState>& wormsList)
{
    for(auto& wormState : wormsList)
    {
        if((wormState.getX()-xPos)*(wormState.getX()-xPos) + (wormState.getY()-yPos)*(wormState.getY()-yPos) < (animationRadius-10)*(animationRadius-10))
            wormState.decreaseHealth(2.0);
    }
}
