#ifndef VECTOR2D_HPP_INCLUDED
#define VECTOR2D_HPP_INCLUDED

#include <cmath>
#include <windows.h>

class Vector2D
{
	public:
    	Vector2D();
    	virtual ~Vector2D() = default;
    	Vector2D(double x, double y);
    	Vector2D(POINT pt);

    	double getX() const { return _x; }
    	double getY() const { return _y; }
    	POINT getPOINT() const;

    	void SetAngle(double angle);  // postavlja _x i _y projekcije zavisno od ugla (angle)
    	double GetAngle() const; // vra�a trenutnu vrijednost ugla u radijanima, koriste�i projekcije _x i _y

    	void SetLength(double length); // postavlja _x i _y projekcije zavisno od du�ine (length)
    	double GetLength() const; // vra�a trenutnu du�inu vektora, koriste�i projekcije _x i _y

    	Vector2D Add(const Vector2D& other); // dodaje vektor other na trenutni i vra�a novi vektor
    	Vector2D Substract(const Vector2D& other); // oduzima trenutni vektor od vektora other i vra�a novi vektor
    	Vector2D Multiply(int value); // mno�i trenutni vektor sa konstantom value i vra�a novi vektor
    	Vector2D Divide(int value); // dijeli trenutni vektor sa konstantom i vra�a novi vektor

    	void AddTo(const Vector2D& other); // dodaje vektor other na trenutni
    	void SubstractFrom(const Vector2D& other); // oduzima vektor other od trenutnog
        void MultiplyBy(int value);   // mno�i trenutni vektor sa skalarom value
    	void DivideBy(int value); // dijeli trenutni vektor sa skalarom value

	private:
    	double _x;
    	double _y;
};

#endif // VECTOR2D_H_INCLUDED
