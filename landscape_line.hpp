#ifndef LANDSCAPE_LINE_HPP_INCLUDED
#define LANDSCAPE_LINE_HPP_INCLUDED

#include <vector>
#include <iostream>
#include <tuple>
#include <map>
#include <windows.h>
#include "free_fall.hpp"
#include "landspace.hpp"
#include <cmath>

class LandscapeLine{
    public:
        LandscapeLine(POINT startPOINT) : startCoordinateX{startPOINT.x} { points.push_back(startPOINT.y); }
        void addPoint(int point) { points.push_back(point); }
        POINT getLastPoint() const;
        bool isActive(int xPos) const { return xPos == startCoordinateX + points.size(); }
        std::tuple<int,POINT> getNextPoint(POINT pt, bool direction);
        std::tuple<bool,POINT> getPointLocation(std::size_t xPos);

        std::size_t getStartX() const { return startCoordinateX; }

        friend std::ostream& operator<<(std::ostream&, const LandscapeLine& line);


    private:
        std::vector<int> points;
        const int startCoordinateX;
};

std::vector<LandscapeLine> getLandspaceMovingLines(HBITMAP landscapeBITMAP);
void addPointToLine(std::vector<LandscapeLine>& lineList, const std::vector<POINT>& pointList);
std::ostream& operator<<(std::ostream& out, const LandscapeLine& line);

std::tuple<int,POINT> moveLeft(std::vector<LandscapeLine>& landscapeLine, Landscape& landscape, POINT currentPoint);
std::tuple<int,POINT> moveRight(std::vector<LandscapeLine>& landscapeLine, Landscape& landscape, POINT currentPoint);
POINT findWormsTopLocation(HBITMAP landscapeBITMAP, int xPos);
std::tuple<bool,POINT> getFreeFallEndPoint(std::vector<LandscapeLine>& landscapeLine, HBITMAP landscapeBITMAP, POINT currentPoint);



#endif // LANDSCAPE_LAND_HPP_INCLUDED
