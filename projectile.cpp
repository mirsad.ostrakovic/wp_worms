#include "projectile.hpp"

const double ProjectileType::airResistance = -0.005;
const Vector2D ProjectileType::gravitationalAcceleration = Vector2D(0.0, 0.1);
const double ProjectileType::explosionRadius = 50.0;
const double ProjectileType::minimumRange = 5.0;
const double ProjectileType::maximumRange = 20.0;


HBITMAP GetRotatedBitmapNT( HBITMAP hBitmap, float radians, COLORREF clrBack )
{
	// Create a memory DC compatible with the display
	//CDC sourceDC, destDC;
	//sourceDC.CreateCompatibleDC( NULL );
	//destDC.CreateCompatibleDC( NULL );
    HDC sourceHDC, destHDC;
    sourceHDC = CreateCompatibleDC(NULL);
    destHDC = CreateCompatibleDC(NULL);



	// Get logical coordinates
	//BITMAP bm;
	//::GetObject( hBitmap, sizeof( bm ), &bm );

    BITMAP bm;
	//::GetObject( hBitmap, sizeof( bm ), &bm );
    GetObject(hBitmap, sizeof(bm), &bm);

	float cosine = (float)cos(radians);
	float sine = (float)sin(radians);

	// Compute dimensions of the resulting bitmap
	// First get the coordinates of the 3 corners other than origin
	int x1 = (int)(bm.bmHeight * sine);
	int y1 = (int)(bm.bmHeight * cosine);
	int x2 = (int)(bm.bmWidth * cosine + bm.bmHeight * sine);
	int y2 = (int)(bm.bmHeight * cosine - bm.bmWidth * sine);
	int x3 = (int)(bm.bmWidth * cosine);
	int y3 = (int)(-bm.bmWidth * sine);

	int minx = min(0,min(x1, min(x2,x3)));
	int miny = min(0,min(y1, min(y2,y3)));
	int maxx = max(0,max(x1, max(x2,x3)));
	int maxy = max(0,max(y1, max(y2,y3)));

	int w = maxx - minx;
	int h = maxy - miny;

	// Create a bitmap to hold the result
	//HBITMAP hbmResult = ::CreateCompatibleBitmap(CClientDC(NULL), w, h);
 	HDC tmpHDC = GetDC(NULL);
    HBITMAP hbmResult = CreateCompatibleBitmap(tmpHDC, w, h);
    ReleaseDC(NULL, tmpHDC);

	//HBITMAP hbmOldSource = (HBITMAP)::SelectObject( sourceDC.m_hDC, hBitmap );
	//HBITMAP hbmOldDest = (HBITMAP)::SelectObject( destDC.m_hDC, hbmResult );
	HBITMAP hbmOldSource = (HBITMAP)SelectObject(sourceHDC, hBitmap);
	HBITMAP hbmOldDest =  (HBITMAP)SelectObject(destHDC, hbmResult);


	// Draw the background color before we change mapping mode
	//HBRUSH hbrBack = CreateSolidBrush( clrBack );
	//HBRUSH hbrOld = (HBRUSH)::SelectObject( destDC.m_hDC, hbrBack );
	//destDC.PatBlt( 0, 0, w, h, PATCOPY );
	//::DeleteObject( ::SelectObject( destDC.m_hDC, hbrOld ) );

	HBRUSH hbrBack = CreateSolidBrush(clrBack);
	HBRUSH hbrOld = (HBRUSH)SelectObject(destHDC, hbrBack);
	PatBlt(destHDC, 0, 0, w, h, PATCOPY);
	DeleteObject(SelectObject(destHDC, hbrOld));


	// We will use world transform to rotate the bitmap
	//SetGraphicsMode(destDC.m_hDC, GM_ADVANCED);
	SetGraphicsMode(destHDC, GM_ADVANCED);
	XFORM xform;
	xform.eM11 = cosine;
	xform.eM12 = -sine;
	xform.eM21 = sine;
	xform.eM22 = cosine;
	xform.eDx = (float)-minx;
	xform.eDy = (float)-miny;

	//SetWorldTransform( destDC.m_hDC, &xform );
	SetWorldTransform( destHDC, &xform );

	// Now do the actual rotating - a pixel at a time
	//destDC.BitBlt(0,0,bm.bmWidth, bm.bmHeight, &sourceDC, 0, 0, SRCCOPY );
    BitBlt(destHDC, 0, 0, bm.bmWidth, bm.bmHeight, sourceHDC, 0, 0, SRCCOPY);

	// Restore DCs
	//::SelectObject( sourceDC.m_hDC, hbmOldSource );
	//::SelectObject( destDC.m_hDC, hbmOldDest );

    SelectObject(sourceHDC, hbmOldSource);
    SelectObject(destHDC, hbmOldDest);

    DeleteDC(sourceHDC);
    DeleteDC(destHDC);

	return hbmResult;
}






void ProjectileType::draw(HDC bufferHDC, int xPos, int yPos, float radians) const
{
  HDC tmpHDC = CreateCompatibleDC(bufferHDC);

  //int xProjectile = (option % 4) * getWidth();
  //int yProjectile = 0;

  std::cout << "Draw projectile" << std::endl;
  HBITMAP rotatedBitmask = GetRotatedBitmapNT(imgBitmask, radians, RGB(255, 255, 255) );
  HBITMAP rotatedImg = GetRotatedBitmapNT(img, radians, RGB(0, 0, 0) );

  BITMAP imgInfo;
  GetObject(rotatedBitmask, sizeof(imgInfo), &imgInfo);

  HBITMAP oldBITMAP = (HBITMAP)SelectObject(tmpHDC, rotatedBitmask);

  std::cout << "startPos: (" << xPos << "," << yPos << ")   endPos(" << xPos + imgInfo.bmWidth << "," << yPos + imgInfo.bmHeight << ")" << std::endl;
  BitBlt(bufferHDC, xPos, yPos, imgInfo.bmWidth, imgInfo.bmHeight, tmpHDC, 0, 0, SRCAND);

  SelectObject(tmpHDC, rotatedImg);
  BitBlt(bufferHDC, xPos, yPos, imgInfo.bmWidth, imgInfo.bmHeight, tmpHDC, 0, 0, SRCPAINT);

  SelectObject(tmpHDC, oldBITMAP);
  DeleteObject(rotatedBitmask);
  DeleteObject(rotatedImg);
  DeleteDC(tmpHDC);
}



bool ProjectileType::isOverlapping(HBITMAP bitmask, int xPos, int yPos, float radians) const
{

    bool returnValue = false;

    BITMAP bitmapInfo;
    GetObject(bitmask, sizeof(bitmapInfo), &bitmapInfo);

    HBITMAP rotatedBitmask = GetRotatedBitmapNT(img, radians, RGB(0, 0, 0));
    BITMAP rotatedBitmaskInfo;
    GetObject(rotatedBitmask, sizeof(rotatedBitmaskInfo), &rotatedBitmaskInfo);

    HDC hdc = GetDC(NULL);
    HDC sourceHDC = CreateCompatibleDC(hdc);
    HDC destHDC = CreateCompatibleDC(hdc);
    ReleaseDC(NULL, hdc);

    HBITMAP oldSourcBITMAP = (HBITMAP)SelectObject(sourceHDC, bitmask);
    HBITMAP oldDestBITMAP = (HBITMAP)SelectObject(destHDC, rotatedBitmask);



    BYTE* lpPixelsSrc;
    BYTE* lpPixelsDst;

    BITMAPINFO MyBMInfoSrc = {0};
    {
            MyBMInfoSrc.bmiHeader.biSize = sizeof(MyBMInfoSrc.bmiHeader);

            if(0 == GetDIBits(sourceHDC, bitmask, 0, 0, NULL, &MyBMInfoSrc, DIB_RGB_COLORS))
            {
                // error handling
            }

            lpPixelsSrc = new BYTE[MyBMInfoSrc.bmiHeader.biSizeImage];

            MyBMInfoSrc.bmiHeader.biBitCount = 32;
            MyBMInfoSrc.bmiHeader.biCompression = BI_RGB;

            MyBMInfoSrc.bmiHeader.biHeight = abs(MyBMInfoSrc.bmiHeader.biHeight);

            if(0 == GetDIBits(sourceHDC, bitmask, 0, MyBMInfoSrc.bmiHeader.biHeight,
                              lpPixelsSrc, &MyBMInfoSrc, DIB_RGB_COLORS))
            {
                // error handling
            }
    }
    //=============================================================================
    //=============================================================================
    BITMAPINFO MyBMInfoDst = {0};
    {
        MyBMInfoDst.bmiHeader.biSize = sizeof(MyBMInfoDst.bmiHeader);

        if(0 == GetDIBits(destHDC, rotatedBitmask, 0, 0, NULL, &MyBMInfoDst, DIB_RGB_COLORS))
        {
        // error handling
        }

        lpPixelsDst = new BYTE[MyBMInfoDst.bmiHeader.biSizeImage];

        MyBMInfoDst.bmiHeader.biBitCount = 32;
        MyBMInfoDst.bmiHeader.biCompression = BI_RGB;

        MyBMInfoDst.bmiHeader.biHeight = abs(MyBMInfoDst.bmiHeader.biHeight);

        if(0 == GetDIBits(destHDC, rotatedBitmask, 0, MyBMInfoDst.bmiHeader.biHeight,
                          lpPixelsDst, &MyBMInfoDst, DIB_RGB_COLORS))
        {
            // error handling
        }
    }


//if(GetPixel(destHDC, i, j) != RGB(0, 0, 0) && GetPixel(sourceHDC, xPos + i, yPos + j) != RGB(0,0,0))

/*
std::ofstream myfile;
myfile.open ("example.txt");


for ( auto y = 0; y < MyBMInfoDst.bmiHeader.biHeight; y++ )
{
        for ( auto x = 0; x < MyBMInfoDst.bmiHeader.biWidth; x++ )
        {
            int srcIndex = 4*(x + MyBMInfoDst.bmiHeader.biWidth*(MyBMInfoDst.bmiHeader.biHeight - y));
            if(lpPixelsDst[srcIndex] == 0 && lpPixelsDst[srcIndex+1] == 0 && lpPixelsDst[srcIndex+2] == 0)
                 myfile << "1";
            else
                myfile << "0";
        }
        myfile << std::endl << "---------------------------" << std::endl;
}


  myfile.close();
*/


    for ( auto y = 0; y < MyBMInfoDst.bmiHeader.biHeight; y++ )
    {
        for ( auto x = 0; x < MyBMInfoDst.bmiHeader.biWidth; x++ )
        {
            int destIndex = 4*(x + MyBMInfoDst.bmiHeader.biWidth*(MyBMInfoDst.bmiHeader.biHeight - y -1));
            int srcIndex = 4 *(x + xPos + MyBMInfoSrc.bmiHeader.biWidth*(MyBMInfoSrc.bmiHeader.biHeight - y - yPos - 1));


            if(srcIndex < 0 || srcIndex  >= (MyBMInfoSrc.bmiHeader.biSizeImage - 2))
                continue;


            if( (lpPixelsDst[destIndex] != 0 && lpPixelsSrc[srcIndex] != 0) ||
                (lpPixelsDst[destIndex + 1] != 0 && lpPixelsSrc[srcIndex + 1] != 0) ||
                (lpPixelsDst[destIndex + 2] != 0 && lpPixelsSrc[srcIndex + 2] != 0))
            {
                std::cout << "Overlapping at, x: " << xPos + x << " , y: " << yPos + y << std::endl;
                std::cout << "Pixel: " << (int)lpPixelsSrc[srcIndex] << " " << (int)lpPixelsSrc[srcIndex+1] << " " << (int)lpPixelsSrc[srcIndex+2] << std::endl;

                SelectObject(sourceHDC, oldSourcBITMAP);
                SelectObject(destHDC, oldDestBITMAP);
                DeleteDC(sourceHDC);
                DeleteDC(destHDC);
                DeleteObject(rotatedBitmask);
                delete []lpPixelsDst;
                delete []lpPixelsSrc;
                return true;
            }
        }
    }





   // for(auto i = 0u; i < rotatedBitmaskInfo.bmWidth; ++i)
   //     for(auto j = 0u; j < rotatedBitmaskInfo.bmHeight; ++j)
   //         if(GetPixel(destHDC, i, j) != RGB(0, 0, 0) && GetPixel(sourceHDC, xPos + i, yPos + j) != RGB(0,0,0))
  //          {
                //std::cout << "Overlapping on, x: " << i << " y: " << j << std::endl;
                //COLORREF color = GetPixel(destHDC, i, j);
                //std::cout << "Boja: " << (int)GetRValue(color) << " " << (int)GetGValue(color) << " " << (int)GetBValue(color) << std::endl;

                //for(auto j = 0 ; j < rotatedBitmaskInfo.bmHeight; ++j)
                //{
                //     COLORREF color = GetPixel(sourceHDC, xPos + i, yPos + j);
                //    std::cout << "SrcHDC(" << xPos + i << "," << yPos + j << ") " << (int)GetRValue(color) << " " << (int)GetGValue(color) << " " << (int)GetBValue(color) << std::endl;
                //}

                //returnValue = true;
                //goto END;
   //              SelectObject(sourceHDC, oldSourcBITMAP);
   //              SelectObject(destHDC, oldDestBITMAP);
    //             DeleteDC(sourceHDC);
    //             DeleteDC(destHDC);
    //             DeleteObject(rotatedBitmask);
    //            return true;
    //        }



    delete []lpPixelsDst;
    delete []lpPixelsSrc;
    SelectObject(sourceHDC, oldSourcBITMAP);
    SelectObject(destHDC, oldDestBITMAP);
    DeleteDC(sourceHDC);
    DeleteDC(destHDC);
    DeleteObject(rotatedBitmask);
    return false;
}





ProjectileType::ProjectileType(const std::string& imgFilename,
                       const std::string& imgBitmaskFilename,
                       int width, int height):
  img{ (HBITMAP)LoadImage(NULL, imgFilename.c_str(), IMAGE_BITMAP, width, height, LR_LOADFROMFILE) },
  imgBitmask{ (HBITMAP)LoadImage(NULL, imgBitmaskFilename.c_str(), IMAGE_BITMAP, width, height, LR_LOADFROMFILE) }
{
      if(img == NULL)
        throw std::invalid_argument("ProjectileType(): image " + imgFilename + " not found");

      if(imgBitmask == NULL)
         throw std::invalid_argument("ProjectileType(): image " + imgBitmaskFilename + " not found");

      BITMAP imgBitmaskInfo;
      GetObject(imgBitmask, sizeof(imgBitmaskInfo), &imgBitmaskInfo);
      GetObject(img, sizeof(imgInfo), &imgInfo);

      if(imgBitmaskInfo.bmWidth != imgInfo.bmWidth || imgBitmaskInfo.bmHeight != imgInfo.bmHeight)
        throw std::invalid_argument("ProjectileType(): incompatibile dimensions of img and bitmaskImg");
}











void ProjectileState::update()
{
  _velocity.AddTo(_acceleration);
  _velocity.AddTo(projectile.gravitationalAcceleration);

  _velocity.AddTo(Vector2D(_velocity.getX()*projectile.airResistance, _velocity.getY()*projectile.airResistance));
  //Vector2D airResistanceAttenuation(_velocity);
  //airResistanceAttenuation.DivideBy(1.0/airResistance);

  //_velocity.AddTo(airResistanceAttenuation);
  //lastPosition = _position;
  _position.AddTo(_velocity);

}



void ProjectileState::render(HDC hdc)
{
  //int x = _position.GetLength() * cos(_position.GetAngle());
  //int y = _position.GetLength() * sin(_position.GetAngle());

  //std::cout << "Velocity x: " << _velocity.getPosX() << std::endl;
  //std::cout << "Velocity y: " << _velocity.getPosY() << std::endl;
  //std::cout << "Angle: " << _velocity.GetAngle() << std::endl;

  Vector2D directionVector(_velocity.getX(), -_velocity.getY());

  projectile.draw(hdc, _position.getX(), _position.getY(), directionVector.GetAngle() - 3.14159/2);

}


void ProjectileState::checkInput() {}



bool ProjectileState::isHit(HBITMAP bitmask) const
{
    Vector2D directionVector(_velocity.getX(), -_velocity.getY());

    return projectile.isOverlapping(bitmask, _position.getX(), _position.getY(), directionVector.GetAngle() - 3.14159/2) ||
           _position.getY() > background.getHeight();
}


POINT ProjectileState::getTopOfRocketLocation() const
{
   // HDC hdc = DetDC(NULL);
    //HDC tmpHDC = CreateCompatibleDC(hdc);
    //ReleaseDC(NULL, hdc);
    Vector2D directionVector(_velocity.getX(), -_velocity.getY());
    double projectileAngle = directionVector.GetAngle() - 3.14159/2;

    HBITMAP rotatedBitmask = GetRotatedBitmapNT(projectile.imgBitmask, projectileAngle, RGB(255, 255, 255) );
    BITMAP imgInfo;
    GetObject(rotatedBitmask, sizeof(imgInfo), &imgInfo);

    int xPos = _position.getX() + imgInfo.bmWidth/2;
    int yPos = _position.getY() + imgInfo.bmHeight/2;

    std::cout << "Projectile init angle: " << _velocity.GetAngle() << std::endl;
    std::cout << "Direction vector angle: " << directionVector.GetAngle() << std::endl;
    std::cout << "Plot vector angle: " << projectileAngle << std::endl;
    std::cout << "Top of rocket location:" << std::endl;
    std::cout << "x: " << _position.getX() << std::endl;
    std::cout << "y: " << _position.getY() << std::endl;
    std::cout << "Middle of rocket location:" << std::endl;
    std::cout << "x: " << xPos << std::endl;
    std::cout << "y: " << yPos << std::endl;




    xPos += projectile.getHeight()/2 * cos(directionVector.GetAngle());
    yPos -= projectile.getHeight()/2 * sin(directionVector.GetAngle());

    std::cout << "Bottom of rocket location:" << std::endl;
    std::cout << "x: " << xPos << std::endl;
    std::cout << "y: " << yPos << std::endl;


  //HBITMAP oldBITMAP = (HBITMAP)SelectObject(tmpHDC, rotatedBitmask);

  //std::cout << "startPos: (" << xPos << "," << yPos << ")   endPos(" << xPos + imgInfo.bmWidth << "," << yPos + imgInfo.bmHeight << ")" << std::endl;
  //BitBlt(bufferHDC, xPos, yPos, imgInfo.bmWidth, imgInfo.bmHeight, tmpHDC, 0, 0, SRCAND);

  //SelectObject(tmpHDC, rotatedImg);
  //BitBlt(bufferHDC, xPos, yPos, imgInfo.bmWidth, imgInfo.bmHeight, tmpHDC, 0, 0, SRCPAINT);

  //SelectObject(tmpHDC, oldBITMAP);
    DeleteObject(rotatedBitmask);
  //DeleteObject(rotatedImg);
  //DeleteDC(tmpHDC);
    POINT returnPOINT{xPos, yPos};

    return returnPOINT;
}

void ProjectileState::increaseRange()
{
    std::cout << "Velocity len inc: " << _velocity.GetLength() << std::endl;

    _velocity.SetLength(_velocity.GetLength() + 0.2);

    std::cout << "Velocity len after inc: " << _velocity.GetLength() << std::endl;

    if(_velocity.GetLength() > getMaximumRange()) // maximum range
        _velocity.SetLength(getMaximumRange());
}

void ProjectileState::decreaseRange()
{
    std::cout << "Velocity len dec: " << _velocity.GetLength() << std::endl;

    _velocity.SetLength(_velocity.GetLength() - 0.2);

    std::cout << "Velocity len after dec: " << _velocity.GetLength() << std::endl;

    if(_velocity.GetLength() < getMinimumRange()) // minimum range
        _velocity.SetLength(getMinimumRange());
}
