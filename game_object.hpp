#ifndef GAME_OBJECT_HPP_INCLUDED
#define GAME_OBJECT_HPP_INCLUDED

#include <windows.h>
#include "vector2D.hpp"

class GameObject
{
	public:
    	GameObject() = default;
    	virtual ~GameObject() = default;
    	virtual void update() = 0;
    	virtual void render(HDC hdc) = 0;
    	virtual void checkInput() = 0;
    	//virtual bool isOnScreen(RECT rect) = 0;
    	//virtual bool isClicked(POINT pt) = 0;

    	Vector2D& getPositionVec() { return _position; }
    	Vector2D& getVelocityVec() { return _velocity; }
    	Vector2D& getAccelerationVec() { return _acceleration; }
	  // getters
 	 // setters
	protected:
    	Vector2D _position;
    	Vector2D _velocity;
    	Vector2D _acceleration;
};


#endif // GAME_OBJECT_H_INCLUDED
