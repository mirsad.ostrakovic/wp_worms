#ifndef GAME_LOOP_FUNCTIONS_HPP_INCLUDED
#define GAME_LOOP_FUNCTIONS_HPP_INCLUDED

#include <windows.h>
#include "background.hpp"
#include "landspace.hpp"
#include <list>
#include <ctime>
#include <thread>
#include <chrono>
#include "projectile.hpp"
#include "explosion_animation_v1.hpp"
#include "worms.hpp"
#include "health_bar.h"
#include "game_instance.h"
#include "projectile_list.hpp"
#include "landscape_line.hpp"
#include "free_fall.hpp"
#include "game_winner.hpp"
#include "projectile_range_bar.hpp"

void draw(HWND hwnd);
void updateStates(HWND hwnd);
void animate();
void initGameInstance(const std::vector<std::string>& players);

extern GameInstance gameInstance;



#endif // GAME_LOOP_FUNCTIONS_HPP_INCLUDED
