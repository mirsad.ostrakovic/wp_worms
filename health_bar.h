#ifndef HEALTH_BAR_H_INCLUDED
#define HEALTH_BAR_H_INCLUDED

#include <windows.h>
#include <string>

class HealthBar{
    public:
        static void draw(HDC bufferHDC, int xPos, int yPos, int width, int height, COLORREF color, float percent, const std::string& userNickname);
};


#endif // HEALTH_BAR_H_INCLUDED
