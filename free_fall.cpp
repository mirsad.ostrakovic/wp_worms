#include "free_fall.hpp"

const double FreeFallState::airResistance = -0.005;
const Vector2D FreeFallState::gravitationalAcceleration = Vector2D(0.0, 0.08);

FreeFallState::FreeFallState(POINT cp, POINT ep, WormsState& ws):
    currentPosition{cp},
    endPosition{ep},
    velocity{},
    worm{ws}
{
    worm.setFreeFalling();
    if(cp.x != ep.x)
        throw std::invalid_argument("currentPoint.x != endPoint.y");

    if(ep.y < cp.y)
        throw std::invalid_argument("endPoint.y < currentPoint.y");
}

void FreeFallState::animate()
{
  velocity.AddTo(gravitationalAcceleration);
  velocity.AddTo(Vector2D(velocity.getX()*airResistance, velocity.getY()*airResistance));

  currentPosition.AddTo(velocity);

  if(endPosition.getY() < currentPosition.getY())
    currentPosition = endPosition;

    worm.moveToPoint(currentPosition.getPOINT());
}

bool FreeFallState::isFinished()
{
    worm.resetFreeFalling();
    return currentPosition.getY() == endPosition.getY();
}
