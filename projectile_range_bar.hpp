#ifndef PROJECTILE_RANGE_BAR_HPP_INCLUDED
#define PROJECTILE_RANGE_BAR_HPP_INCLUDED

#include <windows.h>
#include <iostream>

class ProjectileRangeBar
{
    public:
        static void draw(HDC bufferHDC, int xPos, int yPos, int width, int height, COLORREF color, const double value, const double minimumValue, const double maximumValue);
};

#endif // PROJECTILE_RANGE_BAR_HPP_INCLUDED
